/*---|Wrecking Ball Engine Class|---*/

#include "WreckingBallEngine.h"

CWBEngine::CWBEngine()
{
	//Set uninitialised values for input and graphics
	//If a problem occurs then they will remain uninitialised and creation will fail (safe)
	m_Input		= 0;
	m_Graphics	= 0;
	m_CPU		= 0;
	m_Timer		= 0;
}

bool CWBEngine::CreateEngine(int screenWidth,int screenHeight)
{
	bool result;

	//Initialise Window
	InitWindow(screenWidth,screenHeight);

	//Initialise Input Handler
	m_Input = new CInputHandler;
	if(!m_Input) return false;
	result = m_Input->Initialise(m_hInstance,m_hWnd,screenWidth,screenHeight);
	if(!result)
	{
		MessageBox(m_hWnd,"Could not initialize the input object.","Error",MB_OK);
		return false;
	}

	m_Graphics = new CGraphics;
	if(!m_Graphics) return false;
	result = m_Graphics->Initialise(screenWidth,screenHeight,m_hWnd);
	if(!result) return false;

	m_CPU = new CCPU;
	if(!m_CPU) return false;
	m_CPU->Initialise();

	m_Timer = new CTimer;
	if(!m_Timer) return false;
	result = m_Timer->Initialise();
	if(!result)
	{
		MessageBox(m_hWnd,"Could not initialize the Timer object.","Error",MB_OK);
		return false;
	}

	//Initialize the message structure.
	ZeroMemory(&msg,sizeof(MSG));

	return true;
}

void CWBEngine::Stop()
{
	//Release all objects in the Engine
	if(m_Graphics)
	{
		m_Graphics->Clean();
		delete m_Graphics;
		m_Graphics = 0;
	}

	if(m_Input)
	{
		m_Input->Clean();
		delete m_Input;
		m_Input = 0;
	}

	if(m_CPU)
	{
		m_CPU->Clean();
		delete m_CPU;
		m_CPU = 0;
	}

	if(m_Timer)
	{
		delete m_Timer;
		m_Timer = 0;
	}

	ShutdownWindow();
}

bool CWBEngine::Frame()
{
	bool result;

	m_Count++;
	if(timeGetTime() >= (m_StartTime + 1000))
	{
		m_FPS = m_Count;
		m_Count = 0;

		m_StartTime = timeGetTime();
	}
	//Check for input
	result = m_Input->Frame();
	if(!result) return false;

	//Process a frame
	result = m_Graphics->Render();
	if(!result) return false;

	return true;
}


void CWBEngine::Resource(LPSTR folderPath)
{
	m_ResourceFolder = folderPath;
	if (m_Graphics) { m_Graphics->Resource(folderPath); }
}

float CWBEngine::GetFPS()
{
	return 1/m_FPS;
}

CCamera* CWBEngine::NewCamera(float x,float y,float z)
{
	CCamera* newCamera = m_Graphics->NewCamera(x,y,z);
	return newCamera;
}

CLight* CWBEngine::NewDirectionalLight()
{
	CLight* newLight = m_Graphics->NewDirectionalLight();
	return newLight;
}

CPointLight* CWBEngine::NewPointLight()
{
	CPointLight* newLight = m_Graphics->NewPointLight();
	return newLight;
}

CMesh* CWBEngine::NewMesh(std::string fileName)
{
	fileName = m_ResourceFolder + fileName;
	CMesh* newMesh = m_Graphics->NewMesh(fileName);
	if(!newMesh)
		msg.message = WM_QUIT;
	return newMesh;
}

CSprite* CWBEngine::NewSprite()
{
	CSprite* newSprite = m_Graphics->NewSprite();
	return newSprite;
}

/////////////////////////////////////////////////////////////
//MODEL FUNCTIONS

IModel* CWBEngine::NewModel(CMesh* mesh)
{
	CModel* tempModel = m_Graphics->NewModel(mesh);
	if(!tempModel)
		return false;

	IModel* newModel = new IModel(tempModel);
	if (!newModel)
		return false;
	return newModel;
}

//void CWBEngine::ModelSetMesh(CModel* model,CMesh* mesh)
//{
//	m_Graphics->ModelSetMesh(model,mesh);
//}
//
//CMesh* CWBEngine::ModelGetMesh(CModel* model) {
//	return m_Graphics->ModelGetMesh(model);
//}
//
//void CWBEngine::ModelGetMatrix(CModel* model,D3DXMATRIX &worldMatrix)
//{
//	m_Graphics->ModelGetMatrix(model,worldMatrix);
//}
//
//void CWBEngine::ModelClean(CModel* model)
//{
//	m_Graphics->ModelClean(model);
//}
//
//void CWBEngine::ModelSetTexture(CModel* model,LPSTR texturefile1,LPSTR texturefile2)
//{
//	m_Graphics->ModelSetTexture(model,texturefile1,texturefile2);
//}
//
//void CWBEngine::ModelSetTexture(CModel* model,CTexture* texture)
//{
//	m_Graphics->ModelSetTexture(model,texture);
//}
//
//void CWBEngine::ModelSetSpecular(CModel* model,LPSTR specularfile)
//{
//	m_Graphics->ModelSetSpecular(model,specularfile);
//}
//
//void CWBEngine::ModelSetSpecular(CModel* model,CTexture* specular)
//{
//	m_Graphics->ModelSetSpecular(model,specular);
//}

CTexture* CWBEngine::NewTexture(LPSTR texturefile1, LPSTR texturefile2, LPSTR texturefile3)
{
	return m_Graphics->NewTexture(texturefile1, texturefile2, texturefile3);
}

bool CWBEngine::KeyHit(Key keyPress)
{
	if(m_Input->KeyDown(keyPress))
		return true;
	return false;
}

float CWBEngine::GetMouseX()
{
	return m_Input->GetMouseX();
}

float CWBEngine::GetMouseY()
{
	return m_Input->GetMouseY();
}

void CWBEngine::GetMouse(float &x,float &y)
{
	return m_Input->GetMouse(x,y);
}

void CWBEngine::MouseSensitivity(float sensitivity)
{
	m_Input->Settings(sensitivity);
}


bool CWBEngine::Quit(Key keyPress)
{
	Frame();

	//Handle the windows messages.
	if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	//Exit if windows says so
	if(msg.message == WM_QUIT)
		return true;

	if(m_Input->EscapeHit())
		return true;

	return false;
}

LRESULT CALLBACK CWBEngine::MessageHandler(HWND hwnd,UINT umsg,WPARAM wparam,LPARAM lparam)
{
	return DefWindowProc(hwnd,umsg,wparam,lparam);
}

void CWBEngine::InitWindow(int& windowWidth,int& windowHeight)
{
	WNDCLASSEX wc;
	DEVMODE ScreenSettings;
	int posX,posY;

	//External pointer to object
	ApplicationHandle = this;

	//Instance of application
	m_hInstance = GetModuleHandle(NULL);

	//Title of window
	m_WindowName = "Wrecking Ball Engine";

	//Setup window (Default setting)
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = m_hInstance;
	wc.hIcon = LoadIcon(NULL,IDI_WINLOGO);
	wc.hIconSm = wc.hIcon;
	wc.hCursor = LoadCursor(NULL,IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = m_WindowName;
	wc.cbSize = sizeof(WNDCLASSEX);

	RegisterClassEx(&wc);

	//Get the actual size of the screen to use in full screen if enabled
	int screenWidth = GetSystemMetrics(SM_CXSCREEN);
	int screenHeight = GetSystemMetrics(SM_CYSCREEN);

	if(FULL_SCREEN)
	{
		windowWidth = screenWidth;
		windowHeight = screenHeight;

		//Set the screen to maximum size of the users desktop and 32bit.
		memset(&ScreenSettings,0,sizeof(ScreenSettings));
		ScreenSettings.dmSize       = sizeof(ScreenSettings);
		ScreenSettings.dmPelsWidth  = (unsigned long)windowWidth;
		ScreenSettings.dmPelsHeight = (unsigned long)windowWidth;
		ScreenSettings.dmBitsPerPel = 32;
		ScreenSettings.dmFields     = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		//Change the display settings to full screen.
		ChangeDisplaySettings(&ScreenSettings,CDS_FULLSCREEN);

		//Position of fullscreen window is top left corner
		posX = posY = 0;
	} else
	{
		posX = (GetSystemMetrics(SM_CXSCREEN) - windowWidth) / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - windowHeight) / 2;
	}

	//Create window with the screen settings defined
	m_hWnd = CreateWindowEx(WS_EX_APPWINDOW,m_WindowName,m_WindowName,
		WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
		posX,posY,windowWidth,windowHeight,NULL,NULL,m_hInstance,NULL);

	//Set window as main focus
	ShowWindow(m_hWnd,SW_SHOW);
	SetForegroundWindow(m_hWnd);
	SetFocus(m_hWnd);

	ShowCursor(false);
}

void CWBEngine::ShutdownWindow()
{
	// Show the mouse cursor.
	ShowCursor(true);

	// Fix the display settings if leaving full screen mode.
	if(FULL_SCREEN)
	{
		ChangeDisplaySettings(NULL,0);
	}

	// Remove the window.
	DestroyWindow(m_hWnd);
	m_hWnd = NULL;

	// Remove the application instance.
	UnregisterClass(m_WindowName,m_hInstance);
	m_hInstance = NULL;

	// Release the pointer to this class.
	ApplicationHandle = NULL;

	return;
}

LRESULT CALLBACK WndProc(HWND hWnd,UINT Message,WPARAM wParam,LPARAM lParam)
{
	switch(Message)
	{
		// Check if the window is being destroyed.
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return 0;
	}

	// Check if the window is being closed.
	case WM_CLOSE:
	{
		PostQuitMessage(0);
		return 0;
	}

	// All other messages pass to the message handler in the system class.
	default:
	{
		return ApplicationHandle->MessageHandler(hWnd,Message,wParam,lParam);
	}
	}
}