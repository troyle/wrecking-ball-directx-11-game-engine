#include "CPU.h"

void CCPU::Initialise()
{
	PDH_STATUS status;
	
	// Initialize the flag indicating whether this object can read the system cpu usage or not.
	m_CanRead = true;

	// Create a query object to poll cpu usage.
	status = PdhOpenQuery(NULL,0,&m_QueryHandle);
	if(status != ERROR_SUCCESS)
		m_CanRead = false;

	// Set query object to poll all cpus in the system.
	status = PdhAddCounter(m_QueryHandle,TEXT("\\Processor(_Total)\\% processor time"),0,&m_CounterHandle);
	if(status != ERROR_SUCCESS)
		m_CanRead = false;

	m_LastSampleTime = GetTickCount();

	m_Usage = 0;
}

void CCPU::Clean()
{
	if(m_CanRead)
		PdhCloseQuery(m_QueryHandle);
}

void CCPU::Frame()
{
	PDH_FMT_COUNTERVALUE value;

	if(m_CanRead)
	{
		if((m_LastSampleTime + 1000) < GetTickCount())
		{
			m_LastSampleTime = GetTickCount();

			PdhCollectQueryData(m_QueryHandle);

			PdhGetFormattedCounterValue(m_CounterHandle,PDH_FMT_LONG,NULL,&value);

			m_Usage = value.longValue;
		}
	}
}

int CCPU::GetCpuPercentage()
{
	int usage;

	if(m_CanRead)
		usage = (int)m_CanRead;
	else
		usage = 0;

	return usage;
}