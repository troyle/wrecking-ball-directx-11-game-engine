#ifndef SHADOWSHADER_H
#define SHADOWSHADER_H

#include <d3d11.h>
#include <D3DX10math.h>
#include <D3DX11async.h>
#include <fstream>

class CShadowShader
{
private:
	struct MatrixBuffer
	{
		D3DXMATRIX worldMatrix;
		D3DXMATRIX viewMatrix;
		D3DXMATRIX projMatrix;
		D3DXMATRIX lightViewMatrix;
		D3DXMATRIX lightProjMatrix;
	};

	struct LightBuffer
	{
		D3DXVECTOR4 ambientColour;
		D3DXVECTOR4 diffuseColour;
	};

	struct LightBuffer2
	{
		D3DXVECTOR3 lightPos;
		float padding;
	};

	ID3D11VertexShader* m_VertexShader;
	ID3D11PixelShader* m_PixelShader;
	ID3D11InputLayout* m_Layout;
	ID3D11SamplerState* m_SampleStateWrap;
	ID3D11SamplerState* m_SampleStateClamp;
	ID3D11Buffer* m_MatrixBuffer;
	ID3D11Buffer* m_LightBuffer;
	ID3D11Buffer* m_LightBuffer2;

	bool InitialiseShader(ID3D11Device*,HWND,LPCSTR,LPCSTR);
	void ShutdownShader();
	void OutputShaderErrorMessage(ID3D10Blob*,HWND,LPCSTR);

	bool SetShaderParameters(ID3D11DeviceContext*,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX,ID3D11ShaderResourceView**,
		ID3D11ShaderResourceView*,D3DXVECTOR3,D3DXVECTOR4,D3DXVECTOR4);
	void RenderShader(ID3D11DeviceContext*,int);

public:
	CShadowShader();
	CShadowShader(CShadowShader&){}
	~CShadowShader(){}

	bool Initialise(ID3D11Device*,HWND,LPCSTR);
	void Shutdown();
	bool Render(ID3D11DeviceContext*,int,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX,ID3D11ShaderResourceView**,
		ID3D11ShaderResourceView*,D3DXVECTOR3,D3DXVECTOR4,D3DXVECTOR4);
};

#endif