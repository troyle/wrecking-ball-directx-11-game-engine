#ifndef TEXT_H
#define TEXT_H

#include "FontHandler.h"
#include "FontShader.h"

class CText
{
private:
	struct Sentence
	{
		ID3D11Buffer *vertexBuffer,*indexBuffer;
		int vertexCount,indexCount,maxLength;
		float r,g,b;
	};

	struct Vertex
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR2 UV;
	};

	CFontHandler*	m_Font;
	CFontShader*	m_FontShader;
	int m_ScreenWidth,m_ScreenHeight;
	D3DXMATRIX		m_BaseViewMatrix;
	Sentence*		m_Sentence1;
	Sentence*		m_Sentence2;

	bool InitialiseSentence(ID3D11Device*,Sentence**,int);
	bool UpdateSentence(ID3D11DeviceContext*,Sentence*,char*,int,int,float,float,float);
	void ReleaseSentence(Sentence**);
	bool RenderSentence(ID3D11DeviceContext*,Sentence*,D3DXMATRIX,D3DXMATRIX);
public:
	CText();
	CText(const CText&){}
	~CText(){}

	bool Initialise(ID3D11Device*,ID3D11DeviceContext*,HWND,int,int,D3DXMATRIX);
	bool Render(ID3D11DeviceContext*,D3DXMATRIX,D3DXMATRIX);
	bool SetMousePosition(ID3D11DeviceContext*,int,int);
	void Clean();

};

#endif