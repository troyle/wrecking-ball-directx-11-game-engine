#include "FontShader.h"

CFontShader::CFontShader()
{
	m_VertexShader		= 0;
	m_PixelShader		= 0;
	m_Layout			= 0;
	m_ConstantBuffer	= 0;
	m_SampleState		= 0;
	m_PixelBuffer		= 0;
}

bool CFontShader::Initialise(ID3D11Device* device, HWND hWnd)
{
	bool result;

	//Initialise vertex and pixel shader
	result = InitialiseShader(device,hWnd,"..//Shaders//Font.vs","..//Shaders//Font.ps");
	if(!result) return false;

	return true;
}

void CFontShader::Clean()
{
	//Shutdown the shaders
	ShutdownShader();
}

bool CFontShader::Render(ID3D11DeviceContext* deviceContext,int indexCount,D3DXMATRIX worldMatrix,
	D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix,ID3D11ShaderResourceView** texture,D3DXVECTOR4 pixelColour)
{
	bool result;

	//Set the shader parameters
	result = SetShaderParameters(deviceContext,worldMatrix,viewMatrix,projMatrix,texture,pixelColour);
	if(!result) return false;

	//Render buffers with shader
	RenderShader(deviceContext,indexCount);

	return true;
}

bool CFontShader::InitialiseShader(ID3D11Device* device,HWND hWnd,LPCSTR vsFilename,LPCSTR psFilename)
{
	HRESULT result;
	ID3D10Blob* errorMessage;
	ID3D10Blob* vsBuffer;
	ID3D10Blob* psBuffer;
	D3D11_INPUT_ELEMENT_DESC polygonLayout[2];
	unsigned int numElements;
	D3D11_BUFFER_DESC constantBufferDesc;
	D3D11_BUFFER_DESC pixelBufferDesc;
	D3D11_SAMPLER_DESC samplerDesc;

	//Initialise pointers
	errorMessage	= 0;
	vsBuffer		= 0;
	psBuffer		= 0;

	//Compile the vertex shaders code
	result = D3DX11CompileFromFile(vsFilename,NULL,NULL,"Vertex","vs_5_0",
		D3D10_SHADER_ENABLE_STRICTNESS,0,NULL,&vsBuffer,&errorMessage,NULL);

	if(FAILED(result))
	{
		//If shader failed
		if(errorMessage)
			OutputShaderErrorMessage(errorMessage,hWnd,vsFilename);
		//If there is nothing in the error message
		else
			MessageBox(hWnd,vsFilename,"Shader file is missing",MB_OK);
		return false;
	}

	// Compile the pixel shader code.
	result = D3DX11CompileFromFile(psFilename,NULL,NULL,"Pixel","ps_5_0",
		D3D10_SHADER_ENABLE_STRICTNESS,0,NULL,&psBuffer,&errorMessage,NULL);

	if(FAILED(result))
	{
		//If shader failed
		if(errorMessage)
			OutputShaderErrorMessage(errorMessage,hWnd,psFilename);
		//If there is nothing in the error message
		else
			MessageBox(hWnd,psFilename,"Shader file is missing",MB_OK);
		return false;
	}

	//Create vertex shader from buffer
	result = device->CreateVertexShader(vsBuffer->GetBufferPointer(),vsBuffer->GetBufferSize(),
		NULL,&m_VertexShader);
	if(FAILED(result)) return false;

	//Create pixel shader from buffer
	result = device->CreatePixelShader(psBuffer->GetBufferPointer(),psBuffer->GetBufferSize(),
		NULL,&m_PixelShader);
	if(FAILED(result)) return false;

	//Setup layout of data into the shader
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "TEXCOORD";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	//Get count of elements
	numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	//Create vertex input layout
	result = device->CreateInputLayout(polygonLayout,numElements,vsBuffer->GetBufferPointer(),
		vsBuffer->GetBufferSize(),&m_Layout);
	if(FAILED(result)) return false;

	//Release buffers
	vsBuffer->Release();
	vsBuffer = 0;

	psBuffer->Release();
	psBuffer = 0;

	//Setup description of the matrix constant buffer
	constantBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	constantBufferDesc.ByteWidth = sizeof(ConstantBuffer);
	constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constantBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	constantBufferDesc.MiscFlags = 0;
	constantBufferDesc.StructureByteStride = 0;

	//Create constant buffer pointer
	result = device->CreateBuffer(&constantBufferDesc,NULL,&m_ConstantBuffer);
	if(FAILED(result)) return false;

	//Create texture sampler description
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	result = device->CreateSamplerState(&samplerDesc,&m_SampleState);
	if(FAILED(result)) return false;

	//Setup description of the camera buffer
	pixelBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	pixelBufferDesc.ByteWidth = sizeof(PixelBuffer);
	pixelBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	pixelBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	pixelBufferDesc.MiscFlags = 0;
	pixelBufferDesc.StructureByteStride = 0;

	result = device->CreateBuffer(&pixelBufferDesc,NULL,&m_PixelBuffer);
	if(FAILED(result)) return false;

	return true;
}

void CFontShader::ShutdownShader()
{
	//Release  all the shader stuff
	if(m_PixelBuffer)
	{
		m_PixelBuffer->Release();
		m_PixelBuffer = 0;
	}

	if(m_SampleState)
	{
		m_SampleState->Release();
		m_SampleState = 0;
	}

	if(m_ConstantBuffer)
	{
		m_ConstantBuffer->Release();
		m_ConstantBuffer = 0;
	}

	if(m_Layout)
	{
		m_Layout->Release();
		m_Layout = 0;
	}

	if(m_PixelShader)
	{
		m_PixelShader->Release();
		m_PixelShader = 0;
	}

	if(m_VertexShader)
	{
		m_VertexShader->Release();
		m_VertexShader = 0;
	}
}

void CFontShader::OutputShaderErrorMessage(ID3D10Blob* errorMessage,HWND hWnd,LPCSTR filename)
{
	char* compileErrors;
	unsigned long bufferSize,i;
	std::ofstream fout;

	//Pointer to error message
	compileErrors = (char*)(errorMessage->GetBufferPointer());

	//Get length
	bufferSize = errorMessage->GetBufferSize();

	//Open a file to write to
	fout.open("shader-error.txt");

	//Write
	for(i = 0; i < bufferSize; i++)
		fout<<compileErrors[i];

	//Close
	fout.close();

	//Release
	errorMessage->Release();
	errorMessage = 0;

	//Popup message
	MessageBox(hWnd,"Error compiling shader. Check shader-error.txt for message",filename,MB_OK);
}

bool CFontShader::SetShaderParameters(ID3D11DeviceContext* deviceContext,D3DXMATRIX worldMatrix,
	D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix,ID3D11ShaderResourceView** texture,D3DXVECTOR4 pixelColour)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	ConstantBuffer* constantData;
	PixelBuffer* pixelData;
	unsigned int bufferNumber;

	//Transpose matrices
	D3DXMatrixTranspose(&worldMatrix,&worldMatrix);
	D3DXMatrixTranspose(&viewMatrix,&viewMatrix);
	D3DXMatrixTranspose(&projMatrix,&projMatrix);

	//Lock the constant buffer
	result = deviceContext->Map(m_ConstantBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(result)) return false;

	//Get pointer
	constantData = (ConstantBuffer*)mappedResource.pData;

	//Copy matricies into constant buffer
	constantData->worldMatrix = worldMatrix;
	constantData->viewMatrix = viewMatrix;
	constantData->projMatrix = projMatrix;

	//Unlock constant buffer
	deviceContext->Unmap(m_ConstantBuffer,0);

	//Set position of constant buffer
	bufferNumber = 0;

	//Set constant buffer in vertex shader
	deviceContext->VSSetConstantBuffers(bufferNumber,1,&m_ConstantBuffer);
	deviceContext->PSSetShaderResources(0,1,texture);

	//Lock the camera buffer
	result = deviceContext->Map(m_PixelBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(result)) return false;

	//Get a pointer to the data in the constant buffer.
	pixelData = (PixelBuffer*)mappedResource.pData;

	//Copy the camera position into the constant buffer.
	pixelData->pixelColour = pixelColour;

	//Unlock the camera buffer.
	deviceContext->Unmap(m_PixelBuffer,0);

	//Set the position of the camera constant buffer in the vertex shader.
	bufferNumber = 0;

	//Now set the camera constant buffer in the vertex shader with the updated values.
	deviceContext->PSSetConstantBuffers(bufferNumber,1,&m_PixelBuffer);

	return true;
}

void CFontShader::RenderShader(ID3D11DeviceContext* deviceContext,int indexCount)
{
	//Set vertex input layout
	deviceContext->IASetInputLayout(m_Layout);

	//Set vertex and pixel shaders
	deviceContext->VSSetShader(m_VertexShader,NULL,0);
	deviceContext->PSSetShader(m_PixelShader,NULL,0);

	//Set sampler state
	deviceContext->PSSetSamplers(0,1,&m_SampleState);

	//Render triangle
	deviceContext->DrawIndexed(indexCount,0,0);
}