#include "Camera.h"

void CCamera::Update()
{
	D3DXVECTOR3 up, pos, lookAt;
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;

	//Setup upward vector
	up.x = 0.0f;
	up.y = 1.0f;
	up.z = 0.0f;

	//Set position
	pos.x = m_Pos.x;
	pos.y = m_Pos.y;
	pos.z = m_Pos.z;

	//Set default look direction
	lookAt.x = 0.0f;
	lookAt.y = 0.0f;
	lookAt.z = 1.0f;

	//Set rotations
	pitch = m_Rot.x * 0.0174532925f;
	yaw = m_Rot.y * 0.0174532925f;
	roll = m_Rot.z * 0.0174532925f;

	//Create rotation matrix
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);

	//Transform into rotation matrix
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);

	//Translate rotated camera to location of the viewer
	lookAt = pos + lookAt;

	//Create the view matrix
	D3DXMatrixLookAtLH(&m_ViewMatrix, &pos, &lookAt, &up);
}

void CCamera::RenderReflection(float height)
{
	D3DXVECTOR3 up,pos,lookAt;
	float radians;


	// Setup the vector that points upwards.
	up.x = 0.0f;
	up.y = 1.0f;
	up.z = 0.0f;

	// Setup the position of the camera in the world.
	// For planar reflection invert the Y position of the camera.
	pos.x = m_Pos.x;
	pos.y = -m_Pos.y + (height * 2.0f);
	pos.z = m_Pos.z;

	// Calculate the rotation in radians.
	radians = m_Rot.y * 0.0174532925f;

	// Setup where the camera is looking.
	lookAt.x = sinf(radians) + m_Pos.x;
	lookAt.y = pos.y;
	lookAt.z = cosf(radians) + m_Pos.z;

	// Create the view matrix from the three vectors.
	D3DXMatrixLookAtLH(&m_ReflectMatrix,&pos,&lookAt,&up);
}