#ifndef DEPTHSHADER_H
#define DEPTHSHADER_H

#include <d3d11.h>
#include <D3DX10math.h>
#include <D3DX11async.h>
#include <fstream>

class CDepthShader
{
private:
	struct MatrixBuffer
	{
		D3DXMATRIX worldMatrix;
		D3DXMATRIX viewMatrix;
		D3DXMATRIX projMatrix;
	};

	ID3D11VertexShader* m_VertexShader;
	ID3D11PixelShader*	m_PixelShader;
	ID3D11InputLayout*	m_Layout;
	ID3D11Buffer*		m_MatrixBuffer;

	bool InitialiseShader(ID3D11Device*,HWND,LPCSTR,LPCSTR);
	void ShutdownShader();
	void OutputShaderErrorMessage(ID3D10Blob*,HWND,LPCSTR);

	bool SetShaderParameters(ID3D11DeviceContext*,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX);
	void RenderShader(ID3D11DeviceContext*,int);

public:
	CDepthShader(); 
	CDepthShader(const CDepthShader&){}
	~CDepthShader(){}

	bool Initialise(ID3D11Device*,HWND,LPCSTR);
	void Shutdown();
	bool Render(ID3D11DeviceContext* device,int indexCount,D3DXMATRIX worldMatrix,D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix);
};

#endif