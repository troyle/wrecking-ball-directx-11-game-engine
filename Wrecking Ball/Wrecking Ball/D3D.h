#ifndef CD3D_H
#define CD3D_H

//Linkers
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dx11.lib")
#pragma comment(lib, "d3dx10.lib")

//Includes
#include <dxgi.h>
#include <d3dcommon.h>
#include <d3d11.h>
#include <d3dx10math.h>

class CD3D
{
private:
	bool					m_Vsync;
	int						m_VideoMemory;
	char					m_VideoCardDescription[128];

	float					m_FOV;
	float					m_AspectRatio;

	IDXGISwapChain*			m_SwapChain;
	ID3D11Device*			m_Device;
	ID3D11DeviceContext*	m_DeviceContext;
	ID3D11RenderTargetView* m_RenderTargetView;
	ID3D11Texture2D*		m_DepthStencilBuffer;
	ID3D11DepthStencilView* m_DepthStencilView;
	ID3D11RasterizerState*	m_RasterState;
	D3D11_VIEWPORT			m_Viewport;

	ID3D11DepthStencilState* m_DepthStencilState;
	ID3D11DepthStencilState* m_DepthDisabledStencilState;

	ID3D11BlendState*		m_AlphaBlendingEnable;
	ID3D11BlendState*		m_AlphaBlendingDisable;

	D3DXMATRIX				m_ProjMatrix;
	D3DXMATRIX				m_WorldMatrix;
	D3DXMATRIX 				m_OrthoMatrix;

public:
	CD3D();
	CD3D(const CD3D&){}
	~CD3D(){}

	bool Initialise(int,int,bool,HWND,bool,float,float);
	void Clean();

	void ClearScene(float,float,float,float);
	void Present();

	ID3D11Device* GetDevice(){ return m_Device; }
	ID3D11DeviceContext* GetDeviceContext(){ return m_DeviceContext; }

	void GetProjMatrix(D3DXMATRIX& projMatrix){ projMatrix = m_ProjMatrix; }
	void GetWorldMatrix(D3DXMATRIX& worldMatrix){ worldMatrix = m_WorldMatrix; }
	void GetOrthoMatrix(D3DXMATRIX& orthoMatrix){ orthoMatrix = m_OrthoMatrix; }

	void GetVideoCardInfo(char*,int&);

	void ZBufferOn()  { m_DeviceContext->OMSetDepthStencilState(m_DepthStencilState,1); }
	void ZBufferOff() { m_DeviceContext->OMSetDepthStencilState(m_DepthDisabledStencilState,1); }

	void AlphaBlendingOn();
	void AlphaBlendingOff();

	ID3D11DepthStencilView* GetDepthStencilView();
	void SetBackBufferTarget();
	void ResetViewport();
};

#endif