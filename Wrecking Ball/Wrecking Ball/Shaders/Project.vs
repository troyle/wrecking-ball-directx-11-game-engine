cbuffer MatrixBuffer
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projMatrix;
    matrix viewMatrix2;
    matrix projMatrix2;
};

struct VertexIn
{
    float4 pos : POSITION;
    float2 UV : TEXCOORD0;
    float3 normal : NORMAL;
};

struct PixelIn
{
    float4 pos : SV_POSITION;
    float2 UV : TEXCOORD0;
    float3 normal : NORMAL;
    float4 viewPos : TEXCOORD1;
};

PixelIn Vertex(VertexIn vIn)
{
    PixelIn vOut;
    
	vIn.pos.w = 1.0f;

    vOut.pos = mul(vIn.pos, worldMatrix);
    vOut.pos = mul(vOut.pos, viewMatrix);
    vOut.pos = mul(vOut.pos, projMatrix);

    vOut.viewPos = mul(vIn.pos, worldMatrix);
    vOut.viewPos = mul(vOut.viewPos, viewMatrix2);
    vOut.viewPos = mul(vOut.viewPos, projMatrix2);

    vOut.UV = input.UV;
    
    vOut.normal = mul(vIn.normal, (float3x3)worldMatrix);
	
    vOut.normal = normalize(vOut.normal);

    return vOut;
}