//Globals
cbuffer matrixBuffer
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projMatrix;
};

//Typedefs
struct VertexIn
{
	float4 pos : POSITION;
	float4 colour : COLOR;
};

struct PixelIn
{
	float4 pos : SV_POSITION;
	float4 colour : COLOR;
};

PixelIn Vertex( VertexIn vIn )
{
	PixelIn vOut;

	vIn.pos.w = 1.0f;

	vOut.pos = mul(vIn.pos, worldMatrix);
	vOut.pos = mul(vOut.pos, viewMatrix);
	vOut.pos = mul(vOut.pos, projMatrix);

	vOut.colour = vIn.colour;

	return vOut;
}