cbuffer MatrixBuffer
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projMatrix;
};

struct VertexIn
{
	float4 pos : POSITION;
};

struct PixelIn
{
    float4 pos : SV_POSITION;
    float4 depthPos : TEXTURE;
};

PixelIn Vertex( VertexIn vIn )
{
    PixelIn vOut;

    vIn.pos.w = 1.0f;

    vOut.pos = mul(vIn.pos, worldMatrix);
    vOut.pos = mul(vOut.pos, viewMatrix);
    vOut.pos = mul(vOut.pos, projMatrix);

    vOut.depthPos = vOut.pos;

	return vOut;
}