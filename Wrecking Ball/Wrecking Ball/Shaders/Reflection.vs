cbuffer MatrixBuffer
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projMatrix;
};

cbuffer ReflectionBuffer
{
	matrix reflectionMatrix;
};

struct VertexIn
{
	float4 pos : POSITION;
	float2 UV : TEXCOORD;
};

struct PixelIn
{
	float4 pos : SV_POSITION;
	float2 UV : TEXCORRD0;
	float4 reflectionPos : TEXCOORD1;
};

PixelIn vertex(VertexIn vIn)
{
	PixelIn vOut;
	matrix reflectProjWorld;

	vIn.pos.w = 1.0f;
	
	vOut.pos = mul(vIn.pos, worldMatrix);
	vOut.pos = mul(vOut.pos, viewMatrix);
	vOut.pos = mul(vOut.pos, projMatrix);

	vOut.UV = vIn.UV;

	reflectProjWorld = mul(reflectionMatrix, projMatrix);
	reflectProjWorld = mul(worldMatrix, reflectProjWorld);

	vOut.reflectionPos = mul(vIn.pos,reflectProjectWorld);

	return vOut
}