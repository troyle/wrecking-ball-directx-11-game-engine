cbuffer MatrixBuffer
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projMatrix;
};

cbuffer CameraBuffer
{
    float3 cameraPos;
	float padding;
};

//Typedef
struct VertexIn
{
	float4 pos		: POSITION;
	float2 UV		: TEXCOORD;
	float3 normal	: NORMAL;
};

struct PixelIn
{
	float4 pos		: SV_POSITION;
	float2 UV		: TEXCOORD;
	float3 normal	: NORMAL;
	float3 cameraDir: TEXCOORD1;
};

PixelIn Vertex(VertexIn vIn)
{
	PixelIn vOut;
	float4 worldPos;

	//Convert pos into a position 4x4
	vIn.pos.w = 1.0f;

	//Calculate position of vertex
	vOut.pos = mul(vIn.pos, worldMatrix);
	vOut.pos = mul(vOut.pos, viewMatrix);
	vOut.pos = mul(vOut.pos, projMatrix);

	//Pass through texture
	vOut.UV = vIn.UV;

	//Calculate normal vector
	vOut.normal = mul(vIn.normal, (float3x3)worldMatrix);
	vOut.normal = normalize(vOut.normal);

	//Calculate position of vertex in the world
	worldPos = mul(vIn.pos, worldMatrix);

	//Determine camera direction based on its position and the position of the vertex
	vOut.cameraDir = cameraPos.xyz - worldPos.xyz;
	vOut.cameraDir = normalize(vOut.cameraDir);

	return vOut;
}