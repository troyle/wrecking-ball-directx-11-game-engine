cbuffer MatrixBuffer
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projMatrix;
	matrix lightViewMatrix;
	matrix lightProjMatrix;
};

cbuffer LightBuffer2
{
	float3 lightPos;
	float padding;
};

struct VertexIn
{
	float4 pos : POSITION;
    float2 UV : TEXCOORD;
    float3 normal : NORMAL;
};

struct PixelIn
{
	float4 pos : SV_POSITION;
    float2 UV : TEXCOORD0;
    float3 normal : NORMAL;
    float4 lightViewPos : TEXCOORD1;
    float3 lightPos : TEXCOORD2;
};

PixelIn Vertex(VertexIn vIn)
{
	PixelIn vOut;
	float4 worldPos;

	vIn.pos.w = 1.0f;

	vOut.pos = mul(vIn.pos, worldMatrix);
    vOut.pos = mul(vOut.pos, viewMatrix);
    vOut.pos = mul(vOut.pos, projMatrix);

	vOut.lightViewPos = mul(vIn.pos, worldMatrix);
    vOut.lightViewPos = mul(vOut.lightViewPos, lightViewMatrix);
    vOut.lightViewPos = mul(vOut.lightViewPos, lightProjMatrix);

	vOut.UV = vIn.UV;

	vOut.normal = mul(vIn.normal,(float3x3)worldMatrix);
	vOut.normal = normalize(vOut.normal);

	worldPos = mul(vIn.pos,worldMatrix);

	vOut.lightPos = lightPos.xyz - worldPos.xyz;
	vOut.lightPos = normalize(vOut.lightPos);

	return vOut;
}