cbuffer MatrixBuffer
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projMatrix;
};

//Typedef
struct VertexIn
{
	float4 pos		: POSITION;
	float2 UV		: TEXCOORD;
};

struct PixelIn
{
	float4 pos		: SV_POSITION;
	float2 UV		: TEXCOORD;
};

PixelIn Vertex(VertexIn vIn)
{
	PixelIn vOut;

	//Convert pos into a position 4x4
	vIn.pos.w = 1.0f;

	//Calculate position of vertex
	vOut.pos = mul(vIn.pos, worldMatrix);
	vOut.pos = mul(vOut.pos, viewMatrix);
	vOut.pos = mul(vOut.pos, projMatrix);

	//Pass through texture
	vOut.UV = vIn.UV;

	return vOut;
}