#ifndef POINT_LIGHTING_H
#define POINT_LIGHTING_H

#include "3DObject.h"

#include <D3DX10math.h>

class CPointLight: public I3DObject
{
private:
	D3DXMATRIX m_ViewMatrix;
	D3DXMATRIX m_ProjMatrix;

	D3DXVECTOR3 m_Pos;
	D3DXVECTOR3 m_LookAt;

	D3DXVECTOR4 m_AmbientColour;
	D3DXVECTOR4 m_DiffuseColour;
	D3DXVECTOR3 m_Direction;
	D3DXVECTOR4 m_SpecularColour;
	float		m_SpecularPower;
public:
	CPointLight(){}
	CPointLight(const CPointLight&){}
	~CPointLight(){}

	void SetPos(float x,float y, float z);
	void SetLookAt(float x,float y,float z);

	D3DXVECTOR3 GetPosition() { return m_Pos; }

	void GenerateViewMatrix();
	void GenerateProjMatrix(float farClip,float nearClip);

	void GetViewMatrix(D3DXMATRIX& viewMatrix) { viewMatrix = m_ViewMatrix; }
	void GetProjMatrix(D3DXMATRIX& projMatrix) { projMatrix = m_ProjMatrix; }

	void SetAmbientColour(float r, float g, float b, float a);
	void SetDiffuseColour(float r,float g,float b,float a);
	void SetDirection(float x, float y , float z);
	void SetSpecularColour(float r,float g,float b,float a);
	void SetSpecularPower(float p);

	D3DXVECTOR4 GetAmbientColour()	{ return m_AmbientColour; }
	D3DXVECTOR4 GetDiffuseColour()	{ return m_DiffuseColour; }
	D3DXVECTOR3 GetDirection()		{ return m_Direction; }
	D3DXVECTOR4 GetSpecularColour() { return m_SpecularColour; }
	float		GetSpecularPower()	{ return m_SpecularPower; }
};

#endif