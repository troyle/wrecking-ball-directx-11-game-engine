#include "Mesh.h"
#include <fstream>
#include <string>

CMesh::CMesh()
{
	m_IndexBuffer	= 0;
	m_Mesh			= 0;

	m_FaceCount		= 0;
	m_VertexBuffer	= 0;
	m_TextureCount	= 0;
	m_NormalCount	= 0;
}

bool CMesh::Initialise(ID3D11Device* device, std::string modelFile)
{
	bool result;
	std::string filename = "";
	int i = modelFile.length() - 1;

	do
	{
		filename = modelFile[i] + filename;
		i--;
	}while(modelFile[i] != '/');

	//Load model
	result = LoadModel(modelFile,filename);
	if(!result) return false;

	//Load model
	result = LoadTXTModel(filename + ".wbm");
	if(!result) return false;

	//Initialise vertex and index buffer
	result = InitialiseBuffers(device);
	if(!result) return false;
	return true;
}

void CMesh::Clean()
{
	//Release all buffers
	ShutdownBuffers();

	//Release model
	ReleaseModel();
}

void CMesh::Render(ID3D11DeviceContext* deviceContext)
{
	//Put buffers into graphics pipeline
	RenderBuffers(deviceContext);
}

bool CMesh::InitialiseBuffers(ID3D11Device* device)
{
	Vertex* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;

	m_IndexCount = m_VertexCount = m_FaceCount*3;

	//Create vertex array
	vertices = new Vertex[m_VertexCount];
	if(!vertices) return false;

	//Create index array
	indices = new unsigned long[m_IndexCount];
	if(!indices) return false;

	//Load vertex data
	for(int i=0; i<m_VertexCount; i++)
	{
		vertices[i].pos		= D3DXVECTOR3(m_Mesh[i].x, m_Mesh[i].y, m_Mesh[i].z);
		vertices[i].UV		= D3DXVECTOR2(m_Mesh[i].u, m_Mesh[i].v);
		vertices[i].normal	= D3DXVECTOR3(m_Mesh[i].nx, m_Mesh[i].ny, m_Mesh[i].nz);

		indices[i] = i;
	}

	//Setup description of static vertex buffer
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(Vertex) * m_VertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	//Give subsource structure a pointer to vertex data
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	//Create vertex buffer
	result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_VertexBuffer);
	if(FAILED(result)) return false;

	//Setup description of the static index buffer
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_IndexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	//Give subresource structure a pointer to index data
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	//Create index buffer
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_IndexBuffer);
	if(FAILED(result)) return false;

	//Release arrays
	delete [] vertices;
	vertices = 0;

	delete [] indices;
	indices = 0;

	return true;
}

void CMesh::ShutdownBuffers()
{
	//Release index buffer
	if(m_IndexBuffer)
	{
		m_IndexBuffer->Release();
		m_IndexBuffer = 0;
	}

	//Release vertex buffer
	if(m_VertexBuffer)
	{
		m_VertexBuffer->Release();
		m_VertexBuffer = 0;
	}
}

void CMesh::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;

	//Set stride and offset
	stride = sizeof(Vertex);
	offset = 0;

	//Set vertex buffer to active
	deviceContext->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);

	//Set index buffer to active
	deviceContext->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	//Set type of primative that should be rendered
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

bool CMesh::LoadTXTModel(std::string filename)
{
	std::ifstream file;
	char input;

	//Open file
	file.open(filename);
	if(file.fail()) return false;

	//Read data up to vertex count;
	file.get(input);
	while(input != ':') file.get(input);

	//Read vertex count
	file>>m_VertexCount;
	m_IndexCount = m_VertexCount;

	//Create model using vert count
	m_Mesh = new Mesh[m_VertexCount];
	if(!m_Mesh) return false;

	//Read up to the vertex data
	file.get(input);

	//Read vertex data
	for(int i = 0; i<m_VertexCount; i++)
	{
		file>>m_Mesh[i].x>>m_Mesh[i].y>>m_Mesh[i].z;
		file>>m_Mesh[i].u>>m_Mesh[i].v;
		file>>m_Mesh[i].nx>>m_Mesh[i].ny>>m_Mesh[i].nz;
	}

	//Close file
	file.close();

	return true;
}

bool CMesh::CheckFile(std::string filename)
{
	std::ifstream file;

	//Try and open file
	file.open(filename);

	//If the file exists then carry on
	if(file.good())
	{
		char line;
		file.get(line);
		while(!file.eof())
		{
			if(line == 'v')
			{
				file.get(line);

				if(line == ' ')
					m_VertexCount++;

				else if(line == 't')
					m_TextureCount++;

				else if(line == 'n')
					m_NormalCount++;
			}
			if(line == 'f')
			{
				file.get(line);
				if(line == ' ')
					m_FaceCount++;
			}
				

			//Read rest of line
			while(line != '\n')
				file.get(line);

			//Next line
			file.get(line);
		}
		return true;
	}
	else
	{
		file.clear();
		return false;
	}
}

bool CMesh::LoadModel(std::string modelFile,std::string filename)
{
	struct Coord
	{
		float x, y, z;
	};

	struct UV
	{
		float u, v;
	};

	struct Face
	{
		int v1, t1, n1;
		int v2, t2, n2;
		int v3, t3, n3;
	};

	//Check to see if the file exists
	bool result = CheckFile(modelFile);
	if(!result) return false;

	std::ifstream file;

	char line, slash;

	Coord*	vertex	= new Coord[m_VertexCount];
	UV*		uvs		= new UV[m_TextureCount];
	Coord*	normals	= new Coord[m_NormalCount];
	Face*	face	= new Face[m_FaceCount];

	m_VertexCount = m_TextureCount = m_NormalCount = m_FaceCount = 0;

	file.open(modelFile);

	//Read in the file
	file.get(line);
	while(!file.eof())
	{
		if(line == 'v')
		{
			file.get(line);

			//Data is describing the vertex coordinates
			if(line == ' ')
			{
				file>>vertex[m_VertexCount].x>>vertex[m_VertexCount].y>>vertex[m_VertexCount].z;

				//Invert z axis
				vertex[m_VertexCount].z = vertex[m_VertexCount].z * -1.0f;
				m_VertexCount++;
			}

			//Data is describing the UV coordinates
			if(line == 't')
			{
				file>>uvs[m_TextureCount].u>>uvs[m_TextureCount].v;

				//Invert y axis
				uvs[m_TextureCount].v = 1.0f - uvs[m_TextureCount].v;
				m_TextureCount++;
			}

			//Data is describing the normal coordinates
			if(line == 'n')
			{
				file>>normals[m_NormalCount].x>>normals[m_NormalCount].y>>normals[m_NormalCount].z;

				//Invert z axis
				normals[m_NormalCount].z = normals[m_NormalCount].z * -1.0f;
				m_NormalCount++;
			}
		}

		//Data is describing the face coordinates
		if(line == 'f')
		{
			file.get(line);
			if(line == ' ')
			{
				file>>face[m_FaceCount].v3>>slash>>face[m_FaceCount].t3>>slash>>face[m_FaceCount].n3
					>>face[m_FaceCount].v2>>slash>>face[m_FaceCount].t2>>slash>>face[m_FaceCount].n2
					>>face[m_FaceCount].v1>>slash>>face[m_FaceCount].t1>>slash>>face[m_FaceCount].n1;
				m_FaceCount++;
			}
		}

		//Read rest of line
		while(line != '\n')
			file.get(line);

		//Next line
		file.get(line);
	}
	file.close();

	//Put data into model structure
	int vIndex, tIndex, nIndex = 0;

	std::ofstream fout;
	fout.open(filename + ".wbm");
	//Finally put all the data into the model
	//Since the faces are stored in threes it gets pretty messy
	fout<<"Vertex Count : "<<m_FaceCount*3<<std::endl;

	for(int i=0; i<m_FaceCount; i++)
	{
		vIndex = face[i].v1 - 1;
		tIndex = face[i].t1 - 1;
		nIndex = face[i].n1 - 1;

		fout<< vertex[vIndex].x << ' ' << vertex[vIndex].y << ' ' << vertex[vIndex].z << ' '
		    << uvs[tIndex].u << ' ' << uvs[tIndex].v << ' '
		    << normals[nIndex].x << ' ' << normals[nIndex].y << ' ' << normals[nIndex].z << std::endl;

		vIndex = face[i].v2 - 1;
		tIndex = face[i].t2 - 1;
		nIndex = face[i].n2 - 1;

		fout<< vertex[vIndex].x << ' ' << vertex[vIndex].y << ' ' << vertex[vIndex].z << ' '
		    << uvs[tIndex].u << ' ' << uvs[tIndex].v << ' '
		    << normals[nIndex].x << ' ' << normals[nIndex].y << ' ' << normals[nIndex].z << std::endl;

		vIndex = face[i].v3 - 1;
		tIndex = face[i].t3 - 1;
		nIndex = face[i].n3 - 1;

		fout<< vertex[vIndex].x << ' ' << vertex[vIndex].y << ' ' << vertex[vIndex].z << ' '
		    << uvs[tIndex].u << ' ' << uvs[tIndex].v << ' '
		    << normals[nIndex].x << ' ' << normals[nIndex].y << ' ' << normals[nIndex].z << std::endl;
	}

	/*for(int i = 0; i<m_FaceCount; i++)
	{
		//Use the first set of face data
		vIndex = face[i].v1 - 1;
		tIndex = face[i].t1 - 1;
		nIndex = face[i].n1 - 1;

		m_Model[vIndex].x = vertex[vIndex].x;
		m_Model[vIndex].y = vertex[vIndex].y;
		m_Model[vIndex].z = vertex[vIndex].z;

		m_Model[vIndex].u = uvs[tIndex].u;
		m_Model[vIndex].v = uvs[tIndex].v;

		m_Model[vIndex].nx = normals[nIndex].x;
		m_Model[vIndex].ny = normals[nIndex].y;
		m_Model[vIndex].nz = normals[nIndex].z;

		//Second set of face data
		vIndex = face[i].v2 - 1;
		tIndex = face[i].t2 - 1;
		nIndex = face[i].n2 - 1;

		m_Model[vIndex].x = vertex[vIndex].x;
		m_Model[vIndex].y = vertex[vIndex].y;
		m_Model[vIndex].z = vertex[vIndex].z;

		m_Model[vIndex].u = uvs[tIndex].u;
		m_Model[vIndex].v = uvs[tIndex].v;

		m_Model[vIndex].nx = normals[nIndex].x;
		m_Model[vIndex].ny = normals[nIndex].y;
		m_Model[vIndex].nz = normals[nIndex].z;

		//Third set of face data
		vIndex = face[i].v3 - 1;
		tIndex = face[i].t3 - 1;
		nIndex = face[i].n3 - 1;

		m_Model[vIndex].x = vertex[vIndex].x;
		m_Model[vIndex].y = vertex[vIndex].y;
		m_Model[vIndex].z = vertex[vIndex].z;

		m_Model[vIndex].u = uvs[tIndex].u;
		m_Model[vIndex].v = uvs[tIndex].v;

		m_Model[vIndex].nx = normals[nIndex].x;
		m_Model[vIndex].ny = normals[nIndex].y;
		m_Model[vIndex].nz = normals[nIndex].z;
	}*/
	
	//Clean up the structures
	if(vertex)
	{
		delete vertex;
		vertex = 0;
	}

	if(uvs)
	{
		delete [] uvs;
		uvs = 0;
	}

	if(normals)
	{
		delete [] normals;
		normals = 0;
	}

	if(face)
	{
		delete [] face;
		face = 0;
	}

	return true;
}

void CMesh::ReleaseModel()
{
	if(m_Mesh)
	{
		delete [] m_Mesh;
		m_Mesh = 0;
	}
}