#include "PointLight.h"

void CPointLight::SetPos(float x,float y,float z)
{
	m_Pos = D3DXVECTOR3(x,y,z);
}

void CPointLight::SetLookAt(float x,float y,float z)
{
	m_LookAt = D3DXVECTOR3(x,y,z);
}

void CPointLight::GenerateViewMatrix()
{
	D3DXVECTOR3 up = D3DXVECTOR3(0.0f,1.0f,0.0f);

	D3DXMatrixLookAtLH(&m_ViewMatrix,&m_Pos,&m_LookAt,&up);
}

void CPointLight::GenerateProjMatrix(float farClip,float nearClip)
{
	float fov,aspect;

	fov = (float)D3DX_PI/2.0f;
	aspect = 1.0f;

	D3DXMatrixPerspectiveFovLH(&m_ProjMatrix,fov,aspect,nearClip,farClip);
}

void CPointLight::SetAmbientColour(float r,float g,float b,float a)
{
	m_AmbientColour = D3DXVECTOR4(r,g,b,a);
}

void CPointLight::SetDiffuseColour(float r, float g, float b, float a)
{
	m_DiffuseColour = D3DXVECTOR4(r,g,b,a);
}

void CPointLight::SetDirection(float x, float y, float z)
{
	m_Direction = D3DXVECTOR3(x,y,z);
}

void CPointLight::SetSpecularColour(float r,float g,float b,float a)
{
	m_SpecularColour = D3DXVECTOR4(r,g,b,a);
}

void CPointLight::SetSpecularPower(float p)
{
	m_SpecularPower = p;
}