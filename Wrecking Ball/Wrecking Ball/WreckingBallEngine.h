/*---|Wrecking Ball Engine Header|-----------------*/

//Header Guard
#ifndef WBENGINE_H
#define WBENGINE_H

#define WIN32_LEAN_AND_MEAN

//DirectX Includes
#include <d3d10_1.h>
#include <Windows.h>
#include <mmsystem.h>

//Engine Includes
//#include "WreckingBallInterface.h"
#include "Graphics.h"
#include "InputHandler.h"
#include "CPU.h"
#include "Timer.h"

//Linking
#pragma comment(lib, "winmm.lib")

class CWBEngine//:public IWBEngine
{
private:
	LPCSTR			m_WindowName;
	HINSTANCE		m_hInstance;
	HWND			m_hWnd;
	MSG				msg;
	
	CInputHandler*	m_Input;
	CGraphics*		m_Graphics;
	CCPU*			m_CPU;
	CTimer*			m_Timer;

	LPSTR			m_ResourceFolder;

	float m_FPS;
	int m_Count;
	unsigned long m_StartTime;

	void InitWindow(int&,int&);
	void ShutdownWindow();
public:
	CWBEngine();
	~CWBEngine() {}

	//Initialise engine
	bool CreateEngine(int windowWidth,int windowHeight);

	//The Engine
	bool Frame();
	void Stop();

	CCamera* NewCamera(float x = 0.0f,float y = 0.0f,float z = 0.0f);
	CLight* NewDirectionalLight();
	CPointLight* NewPointLight();
	CMesh* NewMesh(std::string fileName);
	CTexture* NewTexture(LPSTR texturefile1, LPSTR texturefile2 = "", LPSTR texturefile3 = "");
	CSprite* NewSprite();

	IModel* NewModel(CMesh* mesh = nullptr);
	/*void ModelSetMesh(CModel* model,CMesh* mesh);
	CMesh* ModelGetMesh(CModel* model);
	void ModelGetMatrix(CModel* model,D3DXMATRIX &worldMatrix);
	void ModelClean(CModel* model);
	void ModelSetTexture(CModel* model,LPSTR texturefile1,LPSTR texturefile2 = "");
	void ModelSetTexture(CModel* model,CTexture* texture);
	void ModelSetSpecular(CModel* model,LPSTR specularfile);
	void ModelSetSpecular(CModel* model,CTexture* specular);*/

	void Resource(LPSTR folderPath);
	float GetFPS();

	bool KeyHit(Key KeyPress);
	float GetMouseX();
	float GetMouseY();
	void GetMouse(float &x,float&y);

	void MouseSensitivity(float sensitivity);

	bool Quit(Key KeyPress = KEY_ESCAPE);

	LRESULT CALLBACK MessageHandler(HWND,UINT,WPARAM,LPARAM);
};

//Function Prototype
static LRESULT CALLBACK WndProc(HWND,UINT,WPARAM,LPARAM);

//Global
static CWBEngine* ApplicationHandle = 0;

#endif