#ifndef DEBUGWINDOW_H
#define DEBUGWINDOW_H

#include <D3D11.h>
#include <D3DX10math.h>

class CDebugWindow
{
private:
	struct VertexType

	{
		D3DXVECTOR3 pos;
		D3DXVECTOR2 UV;
	};

	ID3D11Buffer *m_VertexBuffer,*m_IndexBuffer;
	int m_VertexCount,m_IndexCount;
	int m_ScreenWidth,m_ScreenHeight;
	int m_BitmapWidth,m_BitmapHeight;
	int m_PrevPosX,m_PrevPosY;

	bool InitialiseBuffers(ID3D11Device*);
	void ShutdownBuffers();
	bool UpdateBuffers(ID3D11DeviceContext*,int,int);
	void RenderBuffers(ID3D11DeviceContext*);

public:
	CDebugWindow();
	CDebugWindow(const CDebugWindow&){}
	~CDebugWindow(){}

	bool Initialise(ID3D11Device*,int,int,int,int);
	void Clean();
	bool Render(ID3D11DeviceContext*,int,int);
	int GetIndexCount();
};

#endif