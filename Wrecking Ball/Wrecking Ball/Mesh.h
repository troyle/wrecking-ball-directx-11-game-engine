#ifndef MESH_H
#define MESH_H

//Includes

#include <d3d11.h>
#include <d3dX10math.h>
#include <fstream>

class CMesh
{
private:
	struct Vertex
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR2 UV;
		D3DXVECTOR3 normal;
	};

	struct Mesh
	{
		float x, y, z;
		float u, v;
		float nx, ny, nz;
	};

	ID3D11Buffer *m_VertexBuffer, *m_IndexBuffer;
	int m_IndexCount;
	int m_VertexCount, m_TextureCount, m_NormalCount, m_FaceCount;
	Mesh* m_Mesh;

	bool InitialiseBuffers(ID3D11Device*);
	void ShutdownBuffers();
	void RenderBuffers(ID3D11DeviceContext*);

	bool LoadTXTModel(std::string);
	bool CheckFile(std::string);

	bool LoadModel(std::string,std::string);
	void ReleaseModel();
public:
	CMesh();
	CMesh(const CMesh&){}
	~CMesh(){}

	bool Initialise(ID3D11Device* device,std::string modelFile);
	void Clean();
	void Render(ID3D11DeviceContext*);

	int GetIndexCount(){ return m_IndexCount; }
};

#endif