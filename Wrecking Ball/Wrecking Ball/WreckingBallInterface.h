/*---|Wrecking Ball Interface Header|-----------------*/

//Header Guard
#ifndef ENGINEINTERFACE_H
#define ENGINEINTERFACE_H

#include "ModelInterface.h"

class WreckingBall
{
public:
	WreckingBall(){}
	//virtual ~WreckingBall() = 0;

	virtual bool CreateEngine(int windowWidth,int windowHeight) = 0;

	virtual bool Frame() = 0;
	virtual void Stop() = 0;

	virtual CCamera* NewCamera(float x,float y,float z) = 0;
	virtual CLight* NewDirectionalLight() = 0;
	virtual CMesh* NewMesh(std::string fileName) = 0;
	virtual CModel* NewModel(CMesh* mesh) = 0;
	virtual CSprite* NewSprite() = 0;

	virtual void Resource(LPSTR folderPath) = 0;
	virtual float GetFPS() = 0;

	virtual bool KeyHit(Key KeyPress) = 0;
	virtual float GetMouseX() = 0;
	virtual float GetMouseY() = 0;
	virtual void GetMouse(float &x,float&y) = 0;

	virtual void MouseSensitivity(float sensitivity) = 0;

	virtual bool Quit(Key KeyPress = KEY_ESCAPE) = 0;
};

#endif