#ifndef OBJECT3D_H
#define OBJECT3D_H

//#include "Structures.h"
#include <d3d10_1.h>
#include <D3DX10math.h>

class I3DObject
{
protected:
	D3DXVECTOR3 m_Pos;
	D3DXVECTOR3 m_Rot;
	D3DXMATRIX m_MatrixRotation;
	D3DXMATRIX m_MatrixScale;

public:
	I3DObject();
	~I3DObject(){};

	void SetPosition(float,float,float);
	void MoveLocal(D3DXVECTOR3);
	D3DXVECTOR3 GetPosition();

	void MoveX(float X);
	void MoveLocalX(float X);
	float GetX();

	void MoveY(float Y);
	void MoveLocalY(float Y);
	float GetY();

	void MoveZ(float Z);
	void MoveLocalZ(float Z);
	float GetZ();

	void SetRotation(float X,float Y,float Z);
	D3DXVECTOR3 GetRotation();

	void RotateX(float X);
	float GetXRotation();

	void RotateY(float Y);
	float GetYRotation();

	void RotateZ(float Z);
	float GetZRotation();

	void GetMatrix(D3DXMATRIX &worldMatrix);
};

#endif