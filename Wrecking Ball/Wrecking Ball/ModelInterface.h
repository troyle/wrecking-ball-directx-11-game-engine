#ifndef MODEL_INTERFACE_H
#define MODEL_INTERFACE_H

#include "Model.h"
//#include "Structures.h"

class IModel
{
private:
	CModel* m_Model;

public:
	IModel() {}
	IModel(CModel* model);
	IModel(const IModel&) {}
	~IModel() {};

	void SetMesh(CMesh*);
	CMesh* GetMesh();

	void SetPosition(float,float,float);
	D3DXVECTOR3 GetPosition();

	void MoveX(float X);
	float GetX();

	void MoveY(float Y);
	float GetY();

	void MoveZ(float Z);
	float GetZ();

	void SetRotation(float X,float Y,float Z);
	D3DXVECTOR3 GetRotation();

	void RotateX(float X);
	float GetXRotation();

	void RotateY(float Y);
	float GetYRotation();

	void RotateZ(float Z);
	float GetZRotation();

	bool SetTexture(LPSTR texture1,LPSTR texture2 = "");
	bool SetTexture(CTexture* texture);

	bool SetSpecular(LPSTR specular);
	bool SetSpecular(CTexture* specular);

	ID3D11ShaderResourceView** GetTexture();
	ID3D11ShaderResourceView** GetSpecular();

	void GetMatrix(D3DXMATRIX &worldMatrix);

	void Clean();
};

#endif