#include "3DObject.h"

I3DObject::I3DObject()
{
	m_Pos.x = m_Pos.y = m_Pos.z = 0.0f;
	m_Rot.x = m_Rot.y = m_Rot.z = 0.0f;

	D3DXMatrixRotationZ(&m_MatrixRotation,m_Rot.z);
	D3DXMatrixRotationX(&m_MatrixRotation,m_Rot.x);
	D3DXMatrixRotationY(&m_MatrixRotation,m_Rot.y);

	D3DXMatrixScaling(&m_MatrixScale,1.0f,1.0f,1.0f);
}

void I3DObject::SetPosition(float X,float Y,float Z)
{
	m_Pos.x = X; m_Pos.y = Y; m_Pos.z = Z;
}
void I3DObject::MoveLocal(D3DXVECTOR3 pos)
{
	m_Pos += D3DXVECTOR3(&m_MatrixRotation._31) * pos.z + D3DXVECTOR3(&m_MatrixRotation._11) * pos.x + D3DXVECTOR3(&m_MatrixRotation._21) * pos.y;
}
D3DXVECTOR3 I3DObject::GetPosition()
{
	return m_Pos;
}

void I3DObject::MoveX(float X)
{
	m_Pos.x += X;
}
void I3DObject::MoveLocalX(float X)
{
	m_Pos += D3DXVECTOR3(&m_MatrixRotation._11) * X;
}
float I3DObject::GetX()
{
	return m_Pos.x;
}

void I3DObject::MoveY(float Y)
{
	m_Pos.y += Y;
}
void I3DObject::MoveLocalY(float Y)
{
	m_Pos += D3DXVECTOR3(&m_MatrixRotation._21) * Y;
}
float I3DObject::GetY()
{
	return m_Pos.y;
}

void I3DObject::MoveZ(float Z)
{
	m_Pos.z += Z;
}
void I3DObject::MoveLocalZ(float Z)
{
	m_Pos += D3DXVECTOR3(&m_MatrixRotation._31) * Z;
}
float I3DObject::GetZ()
{
	return m_Pos.z;
}

void I3DObject::SetRotation(float X,float Y,float Z)
{
	m_Rot.x = X; m_Rot.y = Y; m_Rot.z = Z;

	D3DXMATRIX matrixXRot,matrixYRot,matrixZRot;

	X = X * (D3DX_PI / 180.0f);
	D3DXMatrixRotationX(&matrixXRot,X);
	Y = Y * (D3DX_PI / 180.0f);
	D3DXMatrixRotationY(&matrixYRot,Y);
	Z = Z * (D3DX_PI / 180.0f);
	D3DXMatrixRotationZ(&matrixZRot,Z);

	m_MatrixRotation = matrixZRot * matrixXRot * matrixYRot;
}
D3DXVECTOR3 I3DObject::GetRotation()
{
	return m_Rot;
}

void I3DObject::RotateX(float X)
{
	m_Rot.x += X;

	D3DXMATRIX matrixXRot;

	X = X * (D3DX_PI / 180.0f);
	D3DXMatrixRotationX(&matrixXRot,X);

	m_MatrixRotation *= matrixXRot;
}
float I3DObject::GetXRotation()
{
	return m_Rot.x;
}

void I3DObject::RotateY(float Y)
{
	m_Rot.y += Y;

	D3DXMATRIX matrixYRot;

	Y = Y * (D3DX_PI / 180.0f);
	D3DXMatrixRotationY(&matrixYRot,Y);

	m_MatrixRotation *= matrixYRot;
}
float I3DObject::GetYRotation()
{
	return m_Rot.y;
}

void I3DObject::RotateZ(float Z)
{
	m_Rot.z += Z;

	D3DXMATRIX matrixZRot;

	Z = Z * (D3DX_PI / 180.0f);
	D3DXMatrixRotationZ(&matrixZRot,Z);

	m_MatrixRotation *= matrixZRot;
}
float I3DObject::GetZRotation()
{
	return m_Rot.z;
}

void I3DObject::GetMatrix(D3DXMATRIX &worldMatrix)
{
	D3DXMATRIX matrixTranslation,matrixScaling;
	D3DXMatrixTranslation(&matrixTranslation,m_Pos.x,m_Pos.y,m_Pos.z);

	worldMatrix = m_MatrixScale * m_MatrixRotation * matrixTranslation;
}