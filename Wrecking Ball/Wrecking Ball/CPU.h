#ifndef CPU_H
#define CPU_H

#pragma comment(lib, "pdh.lib")

#include <pdh.h>

class CCPU
{
private:
	bool m_CanRead;
	HQUERY m_QueryHandle;
	HCOUNTER m_CounterHandle;
	unsigned long m_LastSampleTime;
	long m_Usage;

public:
	CCPU(){}
	CCPU(const CCPU&){}
	~CCPU(){}

	void Initialise();
	void Clean();
	void Frame();
	int GetCpuPercentage();

};

#endif