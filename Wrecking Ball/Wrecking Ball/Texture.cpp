#include "Texture.h"


CTexture::CTexture()
{
	m_Texture[0] = 0;
	m_Texture[1] = 0;
	m_Texture[2] = 0;
}

bool CTexture::LoadTexture(ID3D11Device* device,LPSTR filename1,LPSTR filename2,LPSTR filename3)
{
	HRESULT result;

	//Load texture in
	result = D3DX11CreateShaderResourceViewFromFile(device,filename1,NULL,NULL,&m_Texture[0],NULL);
	if(FAILED(result)) return false;

	if (filename2 != "")
	{
		result = D3DX11CreateShaderResourceViewFromFile(device, filename2, NULL, NULL, &m_Texture[1], NULL);
		if (FAILED(result)) return false;

		if (filename3 != "")
		{
			result = D3DX11CreateShaderResourceViewFromFile(device, filename3, NULL, NULL, &m_Texture[2], NULL);
			if (FAILED(result)) return false;
		}
	}
	else
		m_Texture[1] = m_Texture[0];

	return true;
}

void CTexture::Clean()
{
	//Release texture
	if(m_Texture)
	{
		m_Texture[0]->Release();
		m_Texture[0] = 0;

		if(m_Texture[1])
		{
			m_Texture[1]->Release();
			m_Texture[1] = 0;
			
			if (m_Texture[2])
			{
				m_Texture[2]->Release();
				m_Texture[2] = 0;
			}
		}
	}
}