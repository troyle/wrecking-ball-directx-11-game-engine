#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <d3d10_1.h>
#include <Windows.h>

//My Classes
#include "D3D.h"
#include "Camera.h"
#include "Mesh.h"
#include "Shader.h"
#include "DepthShader.h"
#include "ShadowShader.h"
#include "Light.h"
#include "PointLight.h"
#include "Sprite.h"
#include "Text.h"
#include "ModelInterface.h"
#include "Frustrum.h"
#include "DebugWindow.h"
#include "RenderToTexture.h"


//Globals
const bool FULL_SCREEN = false;
const bool VSYNC_ENABLED = true;
const float FAR_CLIP = 100.0f;
const float NEAR_CLIP = 1.0f;

const int SHADOWMAP_WIDTH = 2056;
const int SHADOWMAP_HEIGHT = 2056;

class CGraphics
{
private:
	CD3D*			m_Device;

	CDebugWindow*	m_DebugWindow;

	CCamera*		m_Cameras[1];
	CMesh*			m_Meshs[10];
	CModel*			m_Models[10];
	CLight*			m_Lights[1];
	CPointLight*	m_PointLights[1];
	CSprite*		m_Sprites[1];
	CText*			m_Text[1];
	CTexture*		m_Textures[1];
	CToTexture*		m_ToTexture[1];

	CShader*		m_LightShader;
	CShadowShader*	m_ShadowShader;
	CShader*		m_TextureShader;
	CDepthShader*	m_DepthShader;
	CFrustrum*		m_Frustrum;
	D3DXMATRIX		m_BaseViewMatrix;

	int m_screenHeight;
	int m_screenWidth;
	HWND m_hWnd;
	LPSTR m_ResourceFolder;
	int m_NumModels;
	int m_NumMeshes;

	bool result;

	bool RenderToTexture();
	bool RenderScene();

public:
	CGraphics();
	CGraphics(const CGraphics&){}
	~CGraphics(){}

	bool Initialise(int, int, HWND);
	void Resource(LPSTR);
	void Clean();
	bool Render();
	
	CCamera*	NewCamera(float x=0.0f,float y=0.0f,float z=0.0f);
	CMesh*		NewMesh(std::string fileName);
	CModel*		NewModel(CMesh* mesh = nullptr);
	CTexture*	NewTexture(LPSTR texturefile1, LPSTR texturefile2 = "", LPSTR texturefile3 = "");
	CText*		NewText();
	CLight*		NewDirectionalLight();
	CPointLight*NewPointLight();
	CSprite*	NewSprite();

};

#endif