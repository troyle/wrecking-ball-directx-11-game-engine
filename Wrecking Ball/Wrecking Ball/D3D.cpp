#include "D3D.h"

CD3D::CD3D()
{
	m_SwapChain					= 0;
	m_Device					= 0;
	m_DeviceContext				= 0;
	m_RenderTargetView			= 0;
	m_DepthStencilBuffer		= 0;
	m_DepthStencilView			= 0;
	m_RasterState				= 0;

	m_DepthStencilState			= 0;
	m_DepthDisabledStencilState = 0;

	m_AlphaBlendingEnable		= 0;
	m_AlphaBlendingDisable		= 0;
}

bool CD3D::Initialise(int width, int height, bool vsync, HWND hWnd, bool fullscreen, float farClip, float nearClip)
{
	HRESULT result;
	IDXGIFactory* factory;
	IDXGIAdapter* adapter;
	IDXGIOutput* adapterOutput;
	unsigned int numModes, i, numerator, denominator, stringLength;
	DXGI_MODE_DESC* displayModeList;
	DXGI_ADAPTER_DESC adapterDesc;
	int error;
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	D3D_FEATURE_LEVEL featureLevel;
	ID3D11Texture2D* backBuffer;
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	D3D11_RASTERIZER_DESC rasterDesc;
	float fieldOfView, aspectRatio;

	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	D3D11_DEPTH_STENCIL_DESC depthDisabledStencilDesc;

	D3D11_BLEND_DESC blendStateDesc;

	// Store the vsync setting.
	m_Vsync = vsync;

	//Create directX graphics interface
	result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);
	if(FAILED(result)) return false;

	//Create adapter for graphics card
	result = factory->EnumAdapters(0, &adapter);
	if(FAILED(result)) return false;

	//Enumerate monitor
	result = adapter->EnumOutputs(0, &adapterOutput);
	if(FAILED(result)) return false;

	//Get number of modes that fit the monitor
	result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);
	if(FAILED(result)) return false;

	//Create a list of possible display modes
	displayModeList = new DXGI_MODE_DESC[numModes];
	if(!displayModeList) return false;

	//Fill display mode list
	result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
	if(FAILED(result)) return false;

	for(i=0; i<numModes; i++)
		if(displayModeList[i].Width == (unsigned int)width)
		{
			if(displayModeList[i].Height == (unsigned int)height)
			{
				numerator = displayModeList[i].RefreshRate.Numerator;
				denominator = displayModeList[i].RefreshRate.Denominator;
			}
		}

	//Get video card description
	result = adapter->GetDesc(&adapterDesc);
	if(FAILED(result)) return false;

	//Store video memory
	m_VideoMemory = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

	//Convert name of video card into array
	error = wcstombs_s(&stringLength, m_VideoCardDescription, 128, adapterDesc.Description, 128);
	if(error != 0) return false;

	//Release once used
	delete [] displayModeList;
	displayModeList = 0;

	adapterOutput->Release();
	adapterOutput = 0;

	adapter->Release();
	adapter = 0;

	factory->Release();
	factory = 0;

	//Initialise the swap chain
	ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));

	//Single back buffer
	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Width = width;
	swapChainDesc.BufferDesc.Height = height;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

	//Set refresh rate of back buffer
	if(m_Vsync)
	{
		swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator;
	}
	else
	{
		swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	}

	//Set usage
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

	//Handle for window to render to
	swapChainDesc.OutputWindow = hWnd;

	//Multisampling OFF
	swapChainDesc.SampleDesc.Count = 1;
	swapChainDesc.SampleDesc.Quality = 0;

	if(fullscreen)
		swapChainDesc.Windowed = false;
	else
		swapChainDesc.Windowed = true;

	//Scan line ordering
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	//Discard after swapping
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

	//No advanced flags
	swapChainDesc.Flags = 0;

	//Feature level - ONLY DX11 AT THE MOMENT
	featureLevel = D3D_FEATURE_LEVEL_11_0;

	// Create swap chain, direct3d device and device context
	result = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, &featureLevel, 1, 
				D3D11_SDK_VERSION, &swapChainDesc, &m_SwapChain, &m_Device, NULL, &m_DeviceContext);
	if(FAILED(result)) return false;

	//Get back buffer pointer
	result = m_SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer);
	if(FAILED(result)) return false;

	//Create render target view
	result = m_Device->CreateRenderTargetView(backBuffer, NULL, &m_RenderTargetView);
	if(FAILED(result)) return false;

	//Release back buffer pointer
	backBuffer->Release();
	backBuffer = 0;

	//Initialise the depth buffer description
	ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));

	depthBufferDesc.Width = width;
	depthBufferDesc.Height = height;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	//Create texture for the depth buffer
	result = m_Device->CreateTexture2D(&depthBufferDesc, NULL, &m_DepthStencilBuffer);
	if(FAILED(result)) return false;

	//Initialise the stencil state description
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;

	//Operations if pixel is front-facing.
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	//Operations if pixel is back-facing.
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	//Create  depth stencil state
	result = m_Device->CreateDepthStencilState(&depthStencilDesc, &m_DepthStencilState);
	if(FAILED(result)) return false;

	//Set depth stencil state
	m_DeviceContext->OMSetDepthStencilState(m_DepthStencilState, 1);

	//Initailse the depth stencil view.
	ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

	//Set up depth stencil view description.
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	//Create the depth stencil view.
	result = m_Device->CreateDepthStencilView(m_DepthStencilBuffer, &depthStencilViewDesc, &m_DepthStencilView);
	if(FAILED(result)) return false;

	//Bind render target view and depth stencil buffer to render pipeline
	m_DeviceContext->OMSetRenderTargets(1, &m_RenderTargetView, m_DepthStencilView);

	//Setup the raster description
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = true;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	//Create the rasterizer state
	result = m_Device->CreateRasterizerState(&rasterDesc, &m_RasterState);
	if(FAILED(result)) return false;

	//Set the rasterizer state.
	m_DeviceContext->RSSetState(m_RasterState);

	//Setup viewport
	m_Viewport.Width = (float)width;
	m_Viewport.Height = (float)height;
	m_Viewport.MinDepth = 0.0f;
	m_Viewport.MaxDepth = 1.0f;
	m_Viewport.TopLeftX = 0.0f;
	m_Viewport.TopLeftY = 0.0f;

	//Create viewport.
	m_DeviceContext->RSSetViewports(1, &m_Viewport);

	//Setup projection matrix
	m_FOV = (float)D3DX_PI / 4.0f;
	m_AspectRatio = (float)width / (float)height;

	//Create projection matrix
	D3DXMatrixPerspectiveFovLH(&m_ProjMatrix, m_FOV,m_AspectRatio, nearClip, farClip);

	//Initialise world matrix
	D3DXMatrixIdentity(&m_WorldMatrix);

	//Create orthographic projection matrix
	D3DXMatrixOrthoLH(&m_OrthoMatrix, (float)width, (float)height, nearClip, farClip);

	//Initialise the second stencil state description
	ZeroMemory(&depthDisabledStencilDesc,sizeof(depthDisabledStencilDesc));

	depthDisabledStencilDesc.DepthEnable = false;
	depthDisabledStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthDisabledStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

	depthDisabledStencilDesc.StencilEnable = true;
	depthDisabledStencilDesc.StencilReadMask = 0xFF;
	depthDisabledStencilDesc.StencilWriteMask = 0xFF;

	//Operations if pixel is front-facing.
	depthDisabledStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthDisabledStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	//Operations if pixel is back-facing.
	depthDisabledStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthDisabledStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthDisabledStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	//Create  depth stencil state
	result = m_Device->CreateDepthStencilState(&depthDisabledStencilDesc,&m_DepthDisabledStencilState);
	if(FAILED(result)) return false;

	ZeroMemory(&blendStateDesc,sizeof(D3D11_BLEND_DESC));

	blendStateDesc.RenderTarget[0].BlendEnable = TRUE;
	blendStateDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blendStateDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendStateDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendStateDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendStateDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendStateDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendStateDesc.RenderTarget[0].RenderTargetWriteMask = 0x0f;

	result = m_Device->CreateBlendState(&blendStateDesc,&m_AlphaBlendingEnable);
	if(FAILED(result)) return false;

	blendStateDesc.RenderTarget[0].BlendEnable = FALSE;

	result = m_Device->CreateBlendState(&blendStateDesc,&m_AlphaBlendingDisable);
	if(FAILED(result)) return false;
	
	return true;
}

void CD3D::Clean()
{
	//Set to windowed mode or release swapchain will throw an exception
	if(m_SwapChain) m_SwapChain->SetFullscreenState(false, NULL);

	if(m_AlphaBlendingEnable)
	{
		m_AlphaBlendingEnable->Release();
		m_AlphaBlendingEnable = 0;
	}

	if(m_AlphaBlendingDisable)
	{
		m_AlphaBlendingDisable->Release();
		m_AlphaBlendingDisable = 0;
	}

	if(m_DepthDisabledStencilState)
	{
		m_DepthDisabledStencilState->Release();
		m_DepthDisabledStencilState = 0;
	}

	if(m_RasterState)
	{
		m_RasterState->Release();
		m_RasterState = 0;
	}

	if(m_DepthStencilView)
	{
		m_DepthStencilView->Release();
		m_DepthStencilView = 0;
	}

	if(m_DepthStencilState)
	{
		m_DepthStencilState->Release();
		m_DepthStencilState = 0;
	}

	if(m_DepthStencilBuffer)
	{
		m_DepthStencilBuffer->Release();
		m_DepthStencilBuffer = 0;
	}

	if(m_RenderTargetView)
	{
		m_RenderTargetView->Release();
		m_RenderTargetView = 0;
	}

	if(m_DeviceContext)
	{
		m_DeviceContext->Release();
		m_DeviceContext = 0;
	}

	if(m_Device)
	{
		m_Device->Release();
		m_Device = 0;
	}

	if(m_SwapChain)
	{
		m_SwapChain->Release();
		m_SwapChain = 0;
	}
}

void CD3D::ClearScene(float red, float green, float blue, float alpha)
{
	float color[4];


	// Setup the color to clear the buffer to.
	color[0] = red;
	color[1] = green;
	color[2] = blue;
	color[3] = alpha;

	// Clear the back buffer.
	m_DeviceContext->ClearRenderTargetView(m_RenderTargetView, color);
    
	// Clear the depth buffer.
	m_DeviceContext->ClearDepthStencilView(m_DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	return;
}

void CD3D::Present()
{
	//Present the back buffer to the screen
	if(m_Vsync)
		//Lock to refresh rate
		m_SwapChain->Present(1, 0);
	else
		//Present ASAP
		m_SwapChain->Present(0, 0);
}

void CD3D::GetVideoCardInfo(char* cardName, int& memory)
{
	strcpy_s(cardName, 128, m_VideoCardDescription);
	memory = m_VideoMemory;
}

void CD3D::AlphaBlendingOn()
{
	float blendFactor[4] = {0.0f,0.0f,0.0f,0.0f};

	m_DeviceContext->OMSetBlendState(m_AlphaBlendingEnable,blendFactor,0xffffffff);
}

void CD3D::AlphaBlendingOff()
{
	float blendFactor[4] = {0.0f,0.0f,0.0f,0.0f};
	
	m_DeviceContext->OMSetBlendState(m_AlphaBlendingDisable,blendFactor,0xffffffff);
}

ID3D11DepthStencilView* CD3D::GetDepthStencilView()
{
	return m_DepthStencilView;
}

void CD3D::SetBackBufferTarget()
{
	m_DeviceContext->OMSetRenderTargets(1,&m_RenderTargetView,m_DepthStencilView);
}

void CD3D::ResetViewport()
{
	m_DeviceContext->RSSetViewports(1,&m_Viewport);
}