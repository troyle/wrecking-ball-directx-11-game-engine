/*---|Wrecking Ball Engine Class|---*/
#include "WreckingBall.h"

IWBEngine::IWBEngine()
{
	m_Engine = new CWBEngine();
}

bool IWBEngine::CreateEngine(int screenWidth, int screenHeight)
{
	return m_Engine->CreateEngine(screenWidth,screenHeight);
}

void IWBEngine::Stop()
{
	m_Engine->Stop();
}

bool IWBEngine::Frame()
{
	return m_Engine->Frame();
}

void IWBEngine::Resource(LPSTR folderPath)
{
	m_Engine->Resource(folderPath);
}

float IWBEngine::GetFPS()
{
	return m_Engine->GetFPS();
}

CCamera* IWBEngine::NewCamera(float x, float y, float z)
{
	return m_Engine->NewCamera(x,y,z);
}

CLight* IWBEngine::NewDirectionalLight()
{
	return m_Engine->NewDirectionalLight();
}

CMesh* IWBEngine::NewMesh(std::string fileName)
{
	return m_Engine->NewMesh(fileName);
}

CSprite* IWBEngine::NewSprite()
{
	return m_Engine->NewSprite();
}

CModel* IWBEngine::NewModel(CMesh* mesh)
{
	//return m_Engine->NewModel(mesh);
	return 0;
}

bool IWBEngine::KeyHit(Key keyPress)
{
	return m_Engine->KeyHit(keyPress);
}

float IWBEngine::GetMouseX()
{
	return m_Engine->GetMouseX();
}

float IWBEngine::GetMouseY()
{
	return m_Engine->GetMouseY();
}

void IWBEngine::GetMouse(float &x, float &y)
{
	m_Engine->GetMouse(x,y);
}

void IWBEngine::MouseSensitivity(float sensitivity)
{
	m_Engine->MouseSensitivity(sensitivity);
}

bool IWBEngine::Quit(Key keyPress)
{
	return m_Engine->Quit(keyPress);
}
