#include <iostream>
#include "InputHandler.h"
#include "KeyArray.h"

CInputHandler::CInputHandler()
{
	m_DirectInput	= 0;
	m_Keyboard		= 0;
	m_Mouse			= 0;
	m_Sensitivity	= 0;
}

bool CInputHandler::Initialise(HINSTANCE hInstance,HWND hWnd,int screenWidth,int screenHeight)
{
	HRESULT hr;

	m_ScreenWidth = screenWidth;
	m_ScreenHeight = screenHeight;

	m_MouseX = 0;
	m_MouseY = 0;

	hr = DirectInput8Create(hInstance,DIRECTINPUT_VERSION,IID_IDirectInput8,(void**)&m_DirectInput,NULL);
	if(FAILED(hr)) return false;

	hr = m_DirectInput->CreateDevice(GUID_SysKeyboard,&m_Keyboard,NULL);
	if(FAILED(hr)) return false;

	hr = m_Keyboard->SetDataFormat(&c_dfDIKeyboard);
	if(FAILED(hr)) return false;

	hr = m_Keyboard->SetCooperativeLevel(hWnd,DISCL_FOREGROUND | DISCL_EXCLUSIVE);
	if(FAILED(hr)) return false;

	hr = m_Keyboard->Acquire();
	if(FAILED(hr)) return false;

	hr = m_DirectInput->CreateDevice(GUID_SysMouse,&m_Mouse,NULL);
	if(FAILED(hr)) return false;

	hr = m_Mouse->SetDataFormat(&c_dfDIMouse);
	if(FAILED(hr)) return false;

	hr = m_Mouse->SetCooperativeLevel(hWnd,DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if(FAILED(hr)) return false;

	hr = m_Mouse->Acquire();
	if(FAILED(hr)) return false;

	return true;
}

void CInputHandler::Settings(float sensitivity)
{
	m_Sensitivity = sensitivity * 0.01;
}

void CInputHandler::Clean()
{
	if(m_Mouse)
	{
		m_Mouse->Unacquire();
		m_Mouse->Release();
		m_Mouse = 0;
	}

	if(m_Keyboard)
	{
		m_Keyboard->Unacquire();
		m_Keyboard->Release();
		m_Keyboard = 0;
	}

	if(m_DirectInput)
	{
		m_DirectInput->Release();
		m_DirectInput = 0;
	}
}

bool CInputHandler::Frame()
{
	bool result;

	result = ReadKeyboard();
	if(!result) return false;

	result = ReadMouse();
	if(!result) return false;

	ProcessInput();

	return true;
}

bool CInputHandler::ReadKeyboard()
{
	HRESULT hr;

	hr = m_Keyboard->GetDeviceState(sizeof(m_KeyboardState),(LPVOID)&m_KeyboardState);
	if(FAILED(hr))
	{
		if((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED))
			m_Keyboard->Acquire();
		else
			return false;
	}
	return true;
}

bool CInputHandler::ReadMouse()
{
	HRESULT hr;

	hr = m_Mouse->GetDeviceState(sizeof(DIMOUSESTATE),(LPVOID)&m_MouseState);
	if(FAILED(hr))
	{
		if((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED))
			m_Mouse->Acquire();
		else
			return false;
	}

	return true;
}

void CInputHandler::ProcessInput()
{
	m_MouseX += m_MouseState.lX * m_Sensitivity;
	m_MouseY += m_MouseState.lY * m_Sensitivity;

	/*if(m_MouseX < 0) { m_MouseX = 359; }
	if(m_MouseY < 0) { m_MouseY = 359; }

	if(m_MouseX > 359)	{ m_MouseX = 0; }
	if(m_MouseY > 359)	{ m_MouseY = 0; }*/
}

bool CInputHandler::EscapeHit()
{
	if(m_KeyboardState[DIK_ESCAPE]) return true;

	return false;
}

void CInputHandler::GetMouse(float& mouseX,float& mouseY)
{
	mouseX = m_MouseX;
	mouseY = m_MouseY;
}

float CInputHandler::GetMouseX()
{
	return m_MouseX;
}

float CInputHandler::GetMouseY()
{
	return m_MouseY;
}

bool CInputHandler::KeyDown(Key keyDown)
{
	if(m_KeyboardState[keyArray[keyDown]]) return true;

	return false;
}