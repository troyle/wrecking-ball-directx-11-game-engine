#ifndef FRUSTRUM_H
#define FRUSTRUM_H

#include <D3DX10math.h>

class CFrustrum
{
private:
	D3DXPLANE m_Planes[6];

public:
	CFrustrum() {}
	CFrustrum(const CFrustrum&) {}
	~CFrustrum() {}

	void UpdateFrustrum(float,D3DXMATRIX,D3DXMATRIX);

	bool CheckPoint(float,float,float);
	bool CheckCube(float,float,float,float,float,float);
	bool CheckSphere(float,float,float,float);
};

#endif