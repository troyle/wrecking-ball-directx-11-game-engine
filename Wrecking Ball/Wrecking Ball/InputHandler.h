#ifndef INPUT_H
#define INPUT_H

#define DIRECTINPUT_VERSION 0x0800

#include "Keys.h"

#pragma comment(lib,"dinput8.lib")
#pragma comment(lib,"dxguid.lib")

#include <dinput.h>

class CInputHandler
{
private:
	IDirectInput8*		 m_DirectInput;
	IDirectInputDevice8* m_Keyboard;
	IDirectInputDevice8* m_Mouse;

	unsigned char m_KeyboardState[256];
	DIMOUSESTATE m_MouseState;

	int m_ScreenWidth,m_ScreenHeight;
	float m_MouseX,m_MouseY;

	float m_Sensitivity;

	bool ReadKeyboard();
	bool ReadMouse();
	void ProcessInput();

public:
	CInputHandler();
	CInputHandler(const CInputHandler&){}
	~CInputHandler(){}

	bool Initialise(HINSTANCE,HWND,int,int);
	void Settings(float);
	void Clean();
	bool Frame();

	bool EscapeHit();
	bool KeyDown(Key);

	void GetMouse(float&,float&);
	float GetMouseX();
	float GetMouseY();


};

#endif