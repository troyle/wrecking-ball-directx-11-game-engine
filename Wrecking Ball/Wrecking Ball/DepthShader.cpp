#include "DepthShader.h"

CDepthShader::CDepthShader()
{
	m_VertexShader	= 0;
	m_PixelShader	= 0;
	m_Layout		= 0;
	m_MatrixBuffer	= 0;
}

bool CDepthShader::InitialiseShader(ID3D11Device* device,HWND hwnd,LPCSTR verName,LPCSTR pixName)
{
	HRESULT result;
	ID3D10Blob* errorMessage;
	ID3D10Blob* vertexShaderBuffer;
	ID3D10Blob* pixelShaderBuffer;
	D3D11_INPUT_ELEMENT_DESC polygonLayout[1];
	unsigned int numElements;
	D3D11_BUFFER_DESC matrixBufferDesc;

	errorMessage = 0;
	vertexShaderBuffer = 0;
	pixelShaderBuffer = 0;

	result = D3DX11CompileFromFile(verName,NULL,NULL,"Vertex","vs_5_0",D3D10_SHADER_ENABLE_STRICTNESS,0,NULL,
		&vertexShaderBuffer,&errorMessage,NULL);
	if(FAILED(result))
	{
		if(errorMessage)
			OutputShaderErrorMessage(errorMessage,hwnd,verName);
		else
			MessageBox(hwnd,verName,"Missing Shader File",MB_OK);

		return false;
	}

	result = D3DX11CompileFromFile(pixName,NULL,NULL,"Pixel","ps_5_0",D3D10_SHADER_ENABLE_STRICTNESS,0,NULL,
		&pixelShaderBuffer,&errorMessage,NULL);
	if(FAILED(result))
	{
		if(errorMessage)
			OutputShaderErrorMessage(errorMessage,hwnd,pixName);
		else
			MessageBox(hwnd,pixName,"Missing Shader File",MB_OK);

		return false;
	}

	result = device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(),vertexShaderBuffer->GetBufferSize(),NULL,&m_VertexShader);
	if(FAILED(result)) return false;

	result = device->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(),pixelShaderBuffer->GetBufferSize(),NULL,&m_PixelShader);
	if(FAILED(result)) return false;

	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	result = device->CreateInputLayout(polygonLayout,numElements,vertexShaderBuffer->GetBufferPointer(),vertexShaderBuffer->GetBufferSize(),&m_Layout);
	if(FAILED(result)) return false;

	vertexShaderBuffer->Release();
	vertexShaderBuffer = 0;

	pixelShaderBuffer->Release();
	pixelShaderBuffer = 0;

	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBuffer);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	result = device->CreateBuffer(&matrixBufferDesc,NULL,&m_MatrixBuffer);
	if(FAILED(result)) return false;

	return true;
}

void CDepthShader::ShutdownShader()
{
	if(m_MatrixBuffer)
	{
		m_MatrixBuffer->Release();
		m_MatrixBuffer = 0;
	}

	if(m_Layout)
	{
		m_Layout->Release();
		m_Layout = 0;
	}

	if(m_PixelShader)
	{
		m_PixelShader->Release();
		m_PixelShader = 0;
	}

	if(m_VertexShader)
	{
		m_VertexShader->Release();
		m_VertexShader = 0;
	}
}

void CDepthShader::OutputShaderErrorMessage(ID3D10Blob* errorMessage,HWND hWnd,LPCSTR filename)
{
	char* compileErrors;
	unsigned long bufferSize,i;
	std::ofstream fout;

	//Pointer to error message
	compileErrors = (char*)(errorMessage->GetBufferPointer());

	//Get length
	bufferSize = errorMessage->GetBufferSize();

	//Open a file to write to
	fout.open("shader-error.txt");

	//Write
	for(i = 0; i < bufferSize; i++)
	{
		fout<<compileErrors[i];
	}

	//Close
	fout.close();

	//Release
	errorMessage->Release();
	errorMessage = 0;

	//Popup message
	MessageBox(hWnd,"Error compiling shader. Check shader-error.txt for message",filename,MB_OK);
}

bool CDepthShader::SetShaderParameters(ID3D11DeviceContext* deviceContext,D3DXMATRIX worldMatrix,D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	unsigned int bufferNumber;
	MatrixBuffer* matrixBuffer;

	D3DXMatrixTranspose(&worldMatrix,&worldMatrix);
	D3DXMatrixTranspose(&viewMatrix,&viewMatrix);
	D3DXMatrixTranspose(&projMatrix,&projMatrix);

	result = deviceContext->Map(m_MatrixBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(result)) return false;

	matrixBuffer = (MatrixBuffer*)mappedResource.pData;

	matrixBuffer->worldMatrix = worldMatrix;
	matrixBuffer->viewMatrix = viewMatrix;
	matrixBuffer->projMatrix = projMatrix;

	deviceContext->Unmap(m_MatrixBuffer,0);

	bufferNumber = 0;

	deviceContext->VSSetConstantBuffers(bufferNumber,1,&m_MatrixBuffer);

	return true;
}

void CDepthShader::RenderShader(ID3D11DeviceContext* deviceContext,int indexCount)
{
	deviceContext->IASetInputLayout(m_Layout);

	deviceContext->VSSetShader(m_VertexShader,NULL,0);
	deviceContext->PSSetShader(m_PixelShader,NULL,0);

	deviceContext->DrawIndexed(indexCount,0,0);

	return;
}

bool CDepthShader::Initialise(ID3D11Device* device,HWND hWnd,LPCSTR shaderName)
{
	bool result;
	std::string tempVer,tempPix;
	char* verName;
	char* pixName;

	tempVer	= "..\\Shaders\\" + (std::string)shaderName + ".vs";
	tempPix	= "..\\Shaders\\" + (std::string)shaderName + ".ps";

	verName	= (char*)tempVer.c_str();
	pixName	= (char*)tempPix.c_str();

	//Initialise vertex and pixel shader
	result = InitialiseShader(device,hWnd,verName,pixName);
	if(!result) return false;

	return true;
}

void CDepthShader::Shutdown()
{
	ShutdownShader();
}

bool CDepthShader::Render(ID3D11DeviceContext* deviceContext,int indexCount,D3DXMATRIX worldMatrix,
	D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix)
{
	bool result;

	result = SetShaderParameters(deviceContext,worldMatrix,viewMatrix,projMatrix);
	if(!result) return false;

	RenderShader(deviceContext,indexCount);

	return true;
}