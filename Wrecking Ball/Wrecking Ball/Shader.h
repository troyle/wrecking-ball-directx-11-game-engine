#ifndef SHADER_H
#define SHADER_H

#include "Mesh.h"
#include "Light.h"

//Include
#include <d3d11.h>
#include <D3DX10math.h>
#include <D3DX11async.h>
#include <fstream>

class CShader
{
private:
	struct MatrixBuffer
	{
		D3DXMATRIX worldMatrix;
		D3DXMATRIX viewMatrix;
		D3DXMATRIX projMatrix;
		D3DXMATRIX lightViewMatrix;
		D3DXMATRIX lightProjMatrix;
	};

	struct CameraBuffer
	{
		D3DXVECTOR3 cameraPos;
		float padding;
	};

	struct LightBuffer
	{
		D3DXVECTOR4 ambientColour;
		D3DXVECTOR4 diffuseColour;
		D3DXVECTOR3 lightDir;
		float specularPower;
		D3DXVECTOR4 specularColour;
	};

	struct LightBuffer2
	{
		D3DXVECTOR3 lightPos;
		float padding;
	};

	ID3D11VertexShader* m_VertexShader;
	ID3D11PixelShader*	m_PixelShader;
	ID3D11InputLayout*	m_Layout;
	ID3D11Buffer*		m_MatrixBuffer;
	ID3D11Buffer*		m_LightBuffer;
	ID3D11Buffer*		m_LightBuffer2;
	ID3D11Buffer*		m_CameraBuffer;
	ID3D11SamplerState*	m_SampleStateClamp;
	ID3D11SamplerState*	m_SampleStateWrap;

	bool InitialiseShader(ID3D11Device*, HWND, LPCSTR, LPCSTR);
	void ShutdownShader();
	void OutputShaderErrorMessage(ID3D10Blob*, HWND, LPCSTR);

	bool SetShaderParameters(ID3D11DeviceContext*,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX,ID3D11ShaderResourceView**,
		ID3D11ShaderResourceView**,D3DXVECTOR3,D3DXVECTOR4,D3DXVECTOR4,D3DXVECTOR3,D3DXVECTOR4,float);
	bool SetShaderParameters(ID3D11DeviceContext*,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX,ID3D11ShaderResourceView*);
	bool SetShaderParameters(ID3D11DeviceContext*,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX);

	bool SetShaderParameters(ID3D11DeviceContext*,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX,
		ID3D11ShaderResourceView**,ID3D11ShaderResourceView*,D3DXVECTOR3,D3DXVECTOR4,D3DXVECTOR4);

	void RenderShader(ID3D11DeviceContext*, int);

public:
	CShader();
	CShader(const CShader&){}
	~CShader(){}

	bool Initialise(ID3D11Device*, HWND, LPCSTR);
	void Clean();
	bool Render(ID3D11DeviceContext* deviceContext,int indexCount,D3DXMATRIX worldMatrix,
		D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix,ID3D11ShaderResourceView** texture,
		ID3D11ShaderResourceView** specular,D3DXVECTOR3 cameraPos,CLight* light);
	bool Render(ID3D11DeviceContext* deviceContext,int indexCount,D3DXMATRIX worldMatrix,
		D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix,ID3D11ShaderResourceView* texture);
	bool Render(ID3D11DeviceContext* deviceContext,int indexCount,D3DXMATRIX worldMatrix,
		D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix);
	bool Render(ID3D11DeviceContext* deviceContext,int indexCount,D3DXMATRIX worldMatrix,
		D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix,ID3D11ShaderResourceView* texture,
		ID3D11ShaderResourceView* reflectTexture,D3DXMATRIX reflectMatrix);

	bool Render(ID3D11DeviceContext* deviceContext,int indexCount,D3DXMATRIX worldMatrix,D3DXMATRIX viewMatrix,
		D3DXMATRIX projMatrix,D3DXMATRIX lightViewMatrix,D3DXMATRIX lightProjMatrix,
		ID3D11ShaderResourceView** texture,ID3D11ShaderResourceView* depthMapTexture,D3DXVECTOR3 lightPos,
		D3DXVECTOR4 ambientColour,D3DXVECTOR4 diffuseColour);
};

#endif