#ifndef TIMER_H
#define TIMER_H

#include <windows.h>

class CTimer
{
private:
	INT64 m_Freq;
	float m_TicksPerMs;
	INT64 m_StartTime;
	float m_FrameTime;

public:
	CTimer(){}
	CTimer(const CTimer&){}
	~CTimer(){}

	bool Initialise();
	void Frame();

	float GetTime() { return m_FrameTime; }
};

#endif