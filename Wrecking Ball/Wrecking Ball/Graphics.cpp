#include "Graphics.h"

CGraphics::CGraphics()
{
	m_Device	= 0;
	//Make a stack template that holds all of
	//the objects created for easy deletion on exit
	m_Cameras[0]	= 0;
	m_Meshs[0]		= 0;
	m_Lights[0]		= 0;
	m_Text[0]		= 0;

	m_Sprites[0]	= 0;
	m_ToTexture[0]	= 0;
	m_Frustrum		= 0;
	m_LightShader	= 0;
	m_TextureShader	= 0;
	m_DepthShader	= 0;
	m_DebugWindow = 0;

	int numModels	= 0;
}

bool CGraphics::Initialise(int screenWidth,int screenHeight,HWND hWnd)
{
	bool result;

	m_hWnd = hWnd;
	m_screenHeight = screenHeight;
	m_screenWidth = screenWidth;

	// Create the Direct3D object.
	m_Device = new CD3D;
	if(!m_Device) return false;

	// Initialize the Direct3D object.
	result = m_Device->Initialise(screenWidth,screenHeight,VSYNC_ENABLED,hWnd,FULL_SCREEN,FAR_CLIP,NEAR_CLIP);
	if(!result)
	{
		MessageBox(hWnd,"Could not initialize Direct3D","Error",MB_OK);
		return false;
	}

	m_Frustrum = new CFrustrum();
	if(!m_Frustrum) return false;

	//Create shader object
	m_ShadowShader = new CShadowShader;
	if(!m_ShadowShader) return false;
	result = m_ShadowShader->Initialise(m_Device->GetDevice(),hWnd,"Shadow");
	if(!result)
	{
		MessageBox(hWnd,"Could not initialise shader object","Error",MB_OK);
		return false;
	}

	m_TextureShader = new CShader;
	if(!m_TextureShader) return false;
	result = m_TextureShader->Initialise(m_Device->GetDevice(),hWnd,"Texture");
	if(!result)
	{
		MessageBox(hWnd,"Could not initialise shader object","Error",MB_OK);
		return false;
	}

	m_DepthShader = new CDepthShader;
	if(!m_DepthShader) return false;
	result = m_DepthShader->Initialise(m_Device->GetDevice(),hWnd,"DepthBuffer");
	if(!result)
	{
		MessageBox(hWnd,"Could not initialise shader object","Error",MB_OK);
		return false;
	}

	NewTexture("../Resources/Default.dds");

	//Seperate to own function
	m_ToTexture[0] = new CToTexture;
	if(!m_ToTexture)
		return false;

	result = m_ToTexture[0]->Initialise(m_Device->GetDevice(),SHADOWMAP_WIDTH,SHADOWMAP_HEIGHT,FAR_CLIP,NEAR_CLIP);
	if(!result)
	{
		MessageBox(hWnd,"Could not initialise the render to texture object.","Error",MB_OK);
		return false;
	}

	/*m_Sprites[0] = new CSprite;
	if(!m_Sprites[0])
		return false;

	result = m_Sprites[0]->Initialise(m_Device->GetDevice(),screenWidth,screenHeight,NULL,100,100);
	if(!result) return false;*/

	m_DebugWindow = new CDebugWindow;
	if(!m_DebugWindow) return false;

	result = m_DebugWindow->Initialise(m_Device->GetDevice(),screenWidth,screenHeight,320,180);
	if(!result)
	{
		MessageBox(hWnd,"Could not initialise the debug window object.","Error",MB_OK);
		return false;
	}

	return true;
}

void CGraphics::Resource(LPSTR folderPath)
{
	m_ResourceFolder = folderPath;
}

bool CGraphics::Render()
{
	D3DXMATRIX viewMatrix,projMatrix,worldMatrix,orthoMatrix;

	D3DXMATRIX lightViewMatrix,lightProjMatrix;
	CMesh* tempMesh = nullptr;

	bool result;

	result = RenderToTexture();
	if(!result) return false;

	//Clear buffers
	m_Device->ClearScene(0.2f,0.4f,0.5f,1.0f);

	result = RenderScene();
	if(!result) return false;

	m_Device->ZBufferOff();
	m_Device->AlphaBlendingOn();

	m_Device->GetWorldMatrix(worldMatrix);
	m_Cameras[0]->GetMatrix(viewMatrix);
	m_Device->GetOrthoMatrix(orthoMatrix);

	result = m_DebugWindow->Render(m_Device->GetDeviceContext(),50,50);
	if(!result) return false;

	result = m_TextureShader->Render(m_Device->GetDeviceContext(),m_DebugWindow->GetIndexCount(),worldMatrix,viewMatrix,
		orthoMatrix,m_ToTexture[0]->GetShaderResourceView());

	if(m_Text[0])
	{
		result = m_Text[0]->Render(m_Device->GetDeviceContext(),worldMatrix,orthoMatrix);
		if(!result) return false;
	}

	if(m_Sprites[0])
	{
		result = m_Sprites[0]->Render(m_Device->GetDeviceContext(),50,50);
		if(!result) return false;

		result = m_TextureShader->Render(m_Device->GetDeviceContext(),m_Sprites[0]->GetIndexCount(),worldMatrix,viewMatrix,
			orthoMatrix,m_ToTexture[0]->GetShaderResourceView());
		if(!result) return false;
	}

	m_Device->AlphaBlendingOff();
	m_Device->ZBufferOn();

	//Present back buffer to front buffer
	m_Device->Present();
	return true;
}

void CGraphics::Clean()
{
	if(m_Text[0])
	{
		m_Text[0]->Clean();
		delete m_Text;
		m_Text[0] = 0;
	}

	if(m_ToTexture[0])
	{
		m_ToTexture[0]->Shutdown();
		delete m_ToTexture[0];
		m_ToTexture[0] = 0;
	}

	if(m_Lights[0])
	{
		delete m_Lights[0];
		m_Lights[0] = 0;
	}

	if(m_Sprites[0])
	{
		delete m_Sprites[0];
		m_Sprites[0] = 0;
	}

	if(m_LightShader)
	{
		m_LightShader->Clean();
		delete m_LightShader;
		m_LightShader = 0;
	}

	if(m_TextureShader)
	{
		m_TextureShader->Clean();
		delete m_TextureShader;
		m_TextureShader = 0;
	}

	/*if(m_ReflectShader)
	{
		m_ReflectShader->Clean();
		delete m_ReflectShader;
		m_ReflectShader = 0;
	}*/

	if(m_Meshs[0])
	{
		m_Meshs[0]->Clean();
		delete m_Meshs[0];
		m_Meshs[0] = 0;
	}

	if(m_Frustrum)
	{
		delete m_Frustrum;
		m_Frustrum = 0;
	}

	if(m_Models[0])
	{
		m_Models[0]->Clean();
		delete m_Models[0];
		m_Models[0] = 0;
	}

	if(m_Cameras[0])
	{
		delete m_Cameras[0];
		m_Cameras[0] = 0;
	}

	if(m_DebugWindow)
	{
		m_DebugWindow->Clean();
		delete m_DebugWindow;
		m_DebugWindow = 0;
	}

	if(m_Device)
	{
		m_Device->Clean();
		delete m_Device;
		m_Device = 0;
	}
}

bool CGraphics::RenderToTexture()
{
	D3DXMATRIX worldMatrix,lightViewMatrix,lightProjMatrix,translateMatrix;
	
	bool result;
	float x,y,z;
	CMesh* tempMesh = nullptr;

	m_ToTexture[0]->SetRenderTarget(m_Device->GetDeviceContext());
	m_ToTexture[0]->ClearRenderTarget(m_Device->GetDeviceContext(), 0.0f,0.0f,1.0f,1.0f);

	m_PointLights[0]->GenerateViewMatrix();

	m_Device->GetWorldMatrix(worldMatrix);

	m_PointLights[0]->GetViewMatrix(lightViewMatrix);
	m_PointLights[0]->GetProjMatrix(lightProjMatrix);

	for(int i = 0;i<m_NumModels;i++)
	{
		D3DXVECTOR3 tempPos = m_Models[i]->GetPosition();
		D3DXVECTOR3 position ={tempPos.x,tempPos.y,tempPos.z};
		D3DXMatrixTranslation(&worldMatrix,tempPos.x,tempPos.y,tempPos.z);

		result = true;//m_Frustrum->CheckSphere(position.x,position.y,position.z,2.0f);

		if(result)
		{
			m_Models[i]->GetMatrix(worldMatrix);
			tempMesh = m_Models[i]->GetMesh();
			tempMesh->Render(m_Device->GetDeviceContext());

			//Change to depth shader
			result = m_DepthShader->Render(m_Device->GetDeviceContext(),tempMesh->GetIndexCount(),
				worldMatrix,lightViewMatrix,lightProjMatrix);
			if(!result) return false;

			m_Device->GetWorldMatrix(worldMatrix);
		}
	}

	//result = RenderScene();
	//if(!result) return false;

	m_Device->SetBackBufferTarget();
	m_Device->ResetViewport();

	return true;
}

bool CGraphics::RenderScene()
{
	D3DXMATRIX viewMatrix,projMatrix,worldMatrix,orthoMatrix;
	D3DXMATRIX lightViewMatrix,lightProjMatrix;
	CMesh* tempMesh = nullptr;

	bool result;

	//Generate view matrix
	if(m_Cameras[0])
	{
		m_Cameras[0]->Update();

		//Get matrices
		m_PointLights[0]->GenerateViewMatrix();
		m_Cameras[0]->GetMatrix(viewMatrix);
		m_Device->GetWorldMatrix(worldMatrix);
		m_Device->GetProjMatrix(projMatrix);
		m_Device->GetOrthoMatrix(orthoMatrix);

		m_PointLights[0]->GetViewMatrix(lightViewMatrix);
		m_PointLights[0]->GetProjMatrix(lightProjMatrix);

		//Construct Frustrum
		m_Frustrum->UpdateFrustrum(FAR_CLIP,projMatrix,viewMatrix);

		m_Device->ZBufferOn();
		for(int i = 0;i<m_NumModels;i++)
		{
			D3DXVECTOR3 tempPos = m_Models[i]->GetPosition();
			D3DXVECTOR3 position = {tempPos.x,tempPos.y,tempPos.z};
			tempPos = m_Cameras[0]->GetPosition();
			D3DXVECTOR3 cameraPos = {tempPos.x,tempPos.y,tempPos.z};

			result = true;//m_Frustrum->CheckSphere(position.x,position.y,position.z,2.0f);

			if(result)
			{
				m_Models[i]->GetMatrix(worldMatrix);
				tempMesh = m_Models[i]->GetMesh();
				//Put model into pipeline
				tempMesh->Render(m_Device->GetDeviceContext());

				//Render model
				result = m_ShadowShader->Render(m_Device->GetDeviceContext(),tempMesh->GetIndexCount(),
					worldMatrix,viewMatrix,projMatrix,lightViewMatrix,lightProjMatrix,m_Models[i]->GetTexture(),
					m_ToTexture[0]->GetShaderResourceView(),m_PointLights[0]->GetPosition(),
					m_PointLights[0]->GetAmbientColour(),m_PointLights[0]->GetDiffuseColour());
				if(!result) return false;
			}
		}
	}
}

CCamera* CGraphics::NewCamera(float x,float y,float z)
{
	//Create Camera
	CCamera* camera = new CCamera;
	if(!camera) return false;
	camera->SetPosition(x,y,z);
	camera->Update();
	camera->GetMatrix(m_BaseViewMatrix);

	m_Cameras[0] = camera;

	return camera;
}

CMesh* CGraphics::NewMesh(std::string fileName)
{
	CMesh* mesh = new CMesh;
	if(!mesh) return false;
	result = mesh->Initialise(m_Device->GetDevice(),fileName);
	if(!result)
	{
		MessageBox(m_hWnd,"Could not initialise model object","Error",MB_OK);
		return false;
	}

	m_Meshs[m_NumMeshes] = mesh;
	m_NumMeshes++;

	return mesh;
}

CText* CGraphics::NewText()
{
	CText* text = new CText;
	if(!text) return false;

	result = text->Initialise(m_Device->GetDevice(),m_Device->GetDeviceContext(),m_hWnd,m_screenWidth,
		m_screenHeight,m_BaseViewMatrix);
	if(!result)
	{
		MessageBox(m_hWnd,"Could not initialize the text object.","Error",MB_OK);
		return false;
	}

	m_Text[0] = text;

	return text;
}

CSprite* CGraphics::NewSprite()
{
	CSprite* sprite = new CSprite;
	if(!sprite) return false;
	result = sprite->Initialise(m_Device->GetDevice(),m_screenWidth,m_screenWidth,"../Resources/Default.dds",256,256);
	if(!result)
	{
		MessageBox(m_hWnd,"Could not initialise sprite object","Error",MB_OK);
		return false;
	}

	m_Sprites[0] = sprite;

	return sprite;
}

CLight* CGraphics::NewDirectionalLight()
{
	CLight* light = new CLight;
	if(!light) return false;

	light->SetAmbientColour(0.15f,0.15f,0.15f,1.0f);
	light->SetDiffuseColour(0.7f,0.7f,0.7f,1.0f);
	light->SetDirection(-1.0f,0.0f,1.0f);
	light->SetSpecularColour(1.0f,1.0f,1.0f,1.0f);
	light->SetSpecularPower(32.0f);

	m_Lights[0] = light;

	return light;
}

CPointLight* CGraphics::NewPointLight()
{
	CPointLight* light = new CPointLight;
	if(!light) return false;

	light->SetAmbientColour(0.15f,0.15f,0.15f,1.0f);
	light->SetDiffuseColour(0.7f,0.7f,0.7f,1.0f);
	light->SetDirection(-1.0f,0.0f,1.0f);
	light->SetSpecularColour(1.0f,1.0f,1.0f,1.0f);
	light->SetSpecularPower(32.0f);
	light->SetLookAt(0.0f,0.0f,0.0f);
	light->GenerateProjMatrix(FAR_CLIP,NEAR_CLIP);

	m_PointLights[0] = light;

	return light;
}

CTexture* CGraphics::NewTexture(LPSTR texturefile1,LPSTR texturefile2,LPSTR texturefile3)
{
	bool result;

	//Create texture
	CTexture* texture = new CTexture;
	if(!texture) return false;

	//Load the file into the texture
	result = texture->LoadTexture(m_Device->GetDevice(),texturefile1,texturefile2,texturefile3);
	if(!result) return false;

	m_Textures[0] = texture;

	return texture;
}

CModel* CGraphics::NewModel(CMesh* mesh)
{
	CModel* model = new CModel(mesh);
	if(!model)
	{
		MessageBox(m_hWnd,"Could not initialise model object","Error",MB_OK);
		return false;
	}

	model->SetTexture(m_Textures[0]);

	m_Models[m_NumModels] = model;
	m_NumModels++;


	return model;
}
