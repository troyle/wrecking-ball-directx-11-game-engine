#include "Input.h"

void CInput::Initialise()
{
	int i = 0;

	for(;i<256; i++)
		m_Keys[i] = false;
}

void CInput::KeyDown(unsigned int key)
{
	//If key is hit make it hit in the key array
	m_Keys[key] = true;
}

void CInput::KeyUp(unsigned int key)
{
	//If key is released make it release in the key array
	m_Keys[key] = false;
}

bool CInput::KeyHit(unsigned int key)
{
	return m_Keys[key];
}