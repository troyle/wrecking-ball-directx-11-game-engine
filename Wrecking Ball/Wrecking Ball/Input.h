#ifndef INPUT_H
#define INPUT_H

class CInput
{
private:
	bool m_Keys[256];
public:
	CInput(){}
	CInput(const CInput&){}
	~CInput(){}

	void Initialise();

	void KeyDown(unsigned int);
	void KeyUp(unsigned int);

	bool KeyHit(unsigned int);
};

#endif