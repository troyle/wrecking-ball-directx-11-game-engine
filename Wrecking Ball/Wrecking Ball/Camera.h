#ifndef CAMERA_H
#define CAMERA_H

//Includes
#include "3DObject.h"

class CCamera:public I3DObject
{
private:
	D3DXMATRIX m_ViewMatrix;
	D3DXMATRIX m_ProjMatrix;
	D3DXMATRIX m_ReflectMatrix;

public:
	CCamera(){}
	CCamera(const CCamera&){}
	~CCamera(){}

	void Update();
	void GetMatrix(D3DXMATRIX& viewMatrix){ viewMatrix = m_ViewMatrix; }

	void RenderReflection(float);
	D3DXMATRIX GetReflectionMatrix() { return m_ReflectMatrix; }
};

#endif