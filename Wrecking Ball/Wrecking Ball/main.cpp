#include "WreckingBallEngine.h"

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance, PSTR cmdLine, int CmdShow)
{
	//WreckingBall* WBEngine = new IWBEngine;
	CWBEngine* WBEngine = new CWBEngine;

	WBEngine->CreateEngine(1280,720);
	WBEngine->Resource("../Resources/");
	WBEngine->MouseSensitivity(5);

	CCamera* gameCamera = WBEngine->NewCamera(0,1,-5);

	CPointLight* theSun = WBEngine->NewPointLight();
	theSun->SetPos(30.0f,10.0f,-20.0f);
	theSun->SetLookAt(0.0f,-3.0f,10.0f);

	CMesh* treeMesh = WBEngine->NewMesh("tree.obj");
	IModel* tree = WBEngine->NewModel(treeMesh);
	tree->SetPosition(0.0f,-3.0f,10.0f);

	CMesh* floorMesh = WBEngine->NewMesh("floor.obj");
	IModel* floor = WBEngine->NewModel(floorMesh);
	floor->SetPosition(0.0f,-5.0f,0.0f);

	//CSprite* render = WBEngine->NewSprite();
	//CTexture* grenadeTex = WBEngine->NewTexture("../Resources/stone.dds", "../Resources/dirt.dds", "../Resources/alpha.dds");
	//grenade->SetTexture(grenadeTex);
	
	//Game Loop
	while(!WBEngine->Quit())
	{
		float frameTime = WBEngine->GetFPS();
		float normalSpeed = 5.0f * 1/60;//frameTime;

		if(WBEngine->KeyHit(KEY_LSHIFT)) normalSpeed *= 2;

		if(WBEngine->KeyHit(KEY_W)) gameCamera->MoveLocalZ(normalSpeed);
		if(WBEngine->KeyHit(KEY_S)) gameCamera->MoveLocalZ(-normalSpeed);

		if(WBEngine->KeyHit(KEY_A)) gameCamera->MoveLocalX(-normalSpeed);
		if(WBEngine->KeyHit(KEY_D))	gameCamera->MoveLocalX(normalSpeed);

		if(WBEngine->KeyHit(KEY_E)) tree->RotateY(normalSpeed*10);
		if(WBEngine->KeyHit(KEY_Q)) tree->RotateY(-normalSpeed*10);

		
		float mouseX, mouseY;
		WBEngine->GetMouse(mouseX,mouseY);
		gameCamera->SetRotation(mouseY,mouseX,0);
	}

	WBEngine->Stop();
	delete WBEngine;

	return 0;
}