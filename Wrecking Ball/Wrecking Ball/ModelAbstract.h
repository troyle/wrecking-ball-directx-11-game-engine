#ifndef MODEL_ABSTRACT_H
#define MODEL_ABSTRACT_H

#include "ModelInterface.h"
#include "3DObject.h"

class AModel: public IModel
{
public:
	virtual ~AModel() = 0;

	virtual void SetMesh(CMesh*) = 0;
	virtual CMesh* GetMesh()= 0;

	virtual bool SetTexture(LPSTR texture1,LPSTR texture2 = "") = 0;
	virtual bool SetTexture(CTexture* texture) = 0;

	virtual bool SetSpecular(LPSTR specular) = 0;
	virtual bool SetSpecular(CTexture* specular) = 0;

	virtual ID3D11ShaderResourceView** GetTexture() = 0;
	virtual ID3D11ShaderResourceView** GetSpecular() = 0;

	virtual void GetMatrix(D3DXMATRIX &worldMatrix) = 0;

	virtual void Clean() = 0;
};

#endif