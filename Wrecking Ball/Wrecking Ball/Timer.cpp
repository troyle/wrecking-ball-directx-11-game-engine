#include "Timer.h"

bool CTimer::Initialise()
{
	QueryPerformanceFrequency((LARGE_INTEGER*)&m_Freq);
	if(m_Freq == 0)
		return false;

	m_TicksPerMs = (float)(m_Freq / 1000);

	QueryPerformanceCounter((LARGE_INTEGER*)&m_StartTime);

	return true;
}

void CTimer::Frame()
{
	INT64 currentTime;
	float timeDifference;

	QueryPerformanceCounter((LARGE_INTEGER*)& currentTime);
	timeDifference = (float)(currentTime - m_StartTime);
	m_FrameTime = timeDifference / m_TicksPerMs;
	m_StartTime = currentTime;
}