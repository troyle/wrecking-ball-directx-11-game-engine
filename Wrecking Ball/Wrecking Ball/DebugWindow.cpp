#include "DebugWindow.h"

CDebugWindow::CDebugWindow()
{
	m_VertexBuffer = 0;
	m_IndexBuffer = 0;
}

bool CDebugWindow::Initialise(ID3D11Device* device,int screenWidth,int screenHeight,int bitmapWidth,int bitmapHeight)
{
	bool result;

	m_ScreenWidth = screenWidth;
	m_ScreenHeight = screenHeight;

	m_BitmapWidth = bitmapWidth;
	m_BitmapHeight = bitmapHeight;

	m_PrevPosX = -1;
	m_PrevPosY = -1;

	result = InitialiseBuffers(device);
	if(!result) return false;
}

void CDebugWindow::Clean()
{
	ShutdownBuffers();
}

bool CDebugWindow::Render(ID3D11DeviceContext* deviceContext,int posX,int posY)
{
	bool result;

	result = UpdateBuffers(deviceContext,posX,posY);
	if(!result) return false;

	RenderBuffers(deviceContext);

	return true;
}

int CDebugWindow::GetIndexCount()
{
	return m_IndexCount;
}

bool CDebugWindow::InitialiseBuffers(ID3D11Device* device)
{
	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc,indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData,indexData;
	HRESULT result;
	int i;

	m_VertexCount = 6;

	m_IndexCount = m_VertexCount;

	vertices = new VertexType[m_VertexCount];
	if(!vertices) return false;

	indices = new unsigned long[m_IndexCount];
	if(!indices) return false;

	memset(vertices,0,(sizeof(VertexType) * m_VertexCount));

	for(i=0; i<m_IndexCount; i++)
		indices[i] = i;

	vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_VertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	result = device->CreateBuffer(&vertexBufferDesc,&vertexData,&m_VertexBuffer);
	if(FAILED(result)) return false;

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_IndexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	result = device->CreateBuffer(&indexBufferDesc,&indexData,&m_IndexBuffer);
	if(FAILED(result)) return false;

	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;

	return true;
}

void CDebugWindow::ShutdownBuffers()
{
	if(m_IndexBuffer)
	{
		m_IndexBuffer->Release();
		m_IndexBuffer = 0;
	}

	if(m_VertexBuffer)
	{
		m_VertexBuffer->Release();
		m_VertexBuffer = 0;
	}
}

bool CDebugWindow::UpdateBuffers(ID3D11DeviceContext* deviceContext,int posX,int posY)
{
	float left,right,top,bottom;
	VertexType* vertices;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	VertexType* verticesPtr;
	HRESULT result;


	// If the position we are rendering this bitmap to has not changed then don't update the vertex buffer since it
	// currently has the correct parameters.
	if((posX == m_PrevPosX) && (posY == m_PrevPosY)) return true;

	// If it has changed then update the position it is being rendered to.
	m_PrevPosX = posX;
	m_PrevPosY = posY;

	// Calculate the screen coordinates of the left side of the bitmap.
	left = (float)((m_ScreenWidth / 2) * -1) + (float)posX;

	// Calculate the screen coordinates of the right side of the bitmap.
	right = left + (float)m_BitmapWidth;

	// Calculate the screen coordinates of the top of the bitmap.
	top = (float)(m_ScreenHeight / 2) - (float)posY;

	// Calculate the screen coordinates of the bottom of the bitmap.
	bottom = top - (float)m_BitmapHeight;

	// Create the vertex array.
	vertices = new VertexType[m_VertexCount];
	if(!vertices)
	{
		return false;
	}

	// Load the vertex array with data.
	// First triangle.
	vertices[0].pos = D3DXVECTOR3(left,top,0.0f);  // Top left.
	vertices[0].UV = D3DXVECTOR2(0.0f,0.0f);

	vertices[1].pos = D3DXVECTOR3(right,bottom,0.0f);  // Bottom right.
	vertices[1].UV = D3DXVECTOR2(1.0f,1.0f);

	vertices[2].pos = D3DXVECTOR3(left,bottom,0.0f);  // Bottom left.
	vertices[2].UV = D3DXVECTOR2(0.0f,1.0f);

	// Second triangle.
	vertices[3].pos = D3DXVECTOR3(left,top,0.0f);  // Top left.
	vertices[3].UV = D3DXVECTOR2(0.0f,0.0f);

	vertices[4].pos = D3DXVECTOR3(right,top,0.0f);  // Top right.
	vertices[4].UV = D3DXVECTOR2(1.0f,0.0f);

	vertices[5].pos = D3DXVECTOR3(right,bottom,0.0f);  // Bottom right.
	vertices[5].UV = D3DXVECTOR2(1.0f,1.0f);

	// Lock the vertex buffer so it can be written to.
	result = deviceContext->Map(m_VertexBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(result))
	{
		return false;
	}

	// Get a pointer to the data in the vertex buffer.
	verticesPtr = (VertexType*)mappedResource.pData;

	// Copy the data into the vertex buffer.
	memcpy(verticesPtr,(void*)vertices,(sizeof(VertexType) * m_VertexCount));

	// Unlock the vertex buffer.
	deviceContext->Unmap(m_VertexBuffer,0);

	// Release the vertex array as it is no longer needed.
	delete[] vertices;
	vertices = 0;

	return true;
}

void CDebugWindow::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;

	stride = sizeof(VertexType);
	offset = 0;

	deviceContext->IASetVertexBuffers(0,1,&m_VertexBuffer,&stride,&offset);

	deviceContext->IASetIndexBuffer(m_IndexBuffer,DXGI_FORMAT_R32_UINT,0);

	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}