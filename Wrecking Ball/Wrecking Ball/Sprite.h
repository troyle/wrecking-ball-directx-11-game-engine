#ifndef SPRITE_H
#define SPRITE_H

#include "Texture.h"

#include <d3d11.h>
#include <D3DX10math.h>

class CSprite
{
private:
	struct Vertex
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR2 UV;
	};

	ID3D11Buffer* m_VertexBuffer, *m_IndexBuffer;
	int m_VertexCount,m_IndexCount;
	CTexture* m_Texture;

	int m_ScreenWidth, m_ScreenHeight;
	int m_BitmapWidth, m_BitmapHeight;
	int m_PrevPosX, m_PrevPosY;

	bool InitialiseBuffers(ID3D11Device*);
	void ReleaseBuffers();
	bool UpdateBuffers(ID3D11DeviceContext*,int,int);
	void RenderBuffers(ID3D11DeviceContext*);

	bool LoadTexture(ID3D11Device*,LPSTR);
	void ReleaseTexture();

public:
	CSprite();
	CSprite(const CSprite&) {}
	~CSprite() {}

	bool Initialise(ID3D11Device*,int,int,LPSTR,int,int);
	void Clean();
	bool Render(ID3D11DeviceContext*,int,int);

	int GetIndexCount()						{ return m_IndexCount; }
	ID3D11ShaderResourceView** GetTexture()	{ return m_Texture->GetTexture(); }
};

#endif