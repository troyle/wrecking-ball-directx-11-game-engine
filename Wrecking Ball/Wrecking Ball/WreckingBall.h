/*---|Wrecking Ball Linker|-----------------*/

//Header Guard
#ifndef ENGINEUSER_H
#define ENGINEUSER_H

//Engine Includes
#include "WreckingBallEngine.h"
#include "WreckingBallInterface.h"

class IWBEngine:public WreckingBall
{
private:
	CWBEngine* m_Engine;

public:
	IWBEngine();
	~IWBEngine(){}
	
	//Initialise engine
	bool CreateEngine(int windowWidth, int windowHeight);

	//The Engine
	bool Frame();
	void Stop();

	CCamera* NewCamera(float x = 0.0f,float y = 0.0f,float z = 0.0f);
	CLight* NewDirectionalLight();
	CMesh* NewMesh(std::string fileName);
	CModel* NewModel(CMesh* mesh = nullptr);
	CSprite* NewSprite();
	
	void Resource(LPSTR folderPath);
	float GetFPS();

	bool KeyHit(Key KeyPress);
	float GetMouseX();
	float GetMouseY();
	void GetMouse(float &x, float&y);

	void MouseSensitivity(float sensitivity);

	bool Quit(Key KeyPress = KEY_ESCAPE);
};

#endif