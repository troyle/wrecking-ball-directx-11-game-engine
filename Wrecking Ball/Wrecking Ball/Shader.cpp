#include "Shader.h"

CShader::CShader()
{
	m_VertexShader = 0;
	m_PixelShader = 0;

	m_Layout = 0;
	m_MatrixBuffer = 0;
	m_LightBuffer = 0;
	m_LightBuffer2 = 0;
	m_CameraBuffer = 0;
	m_SampleStateWrap = 0;
	m_SampleStateClamp = 0;
}

bool CShader::Initialise(ID3D11Device* device, HWND hWnd, LPCSTR shaderName)
{
	bool result;
	std::string tempVer, tempPix;
	char* verName;
	char* pixName;
	
	tempVer	= "..\\Shaders\\" + (std::string)shaderName + ".vs";
	tempPix	= "..\\Shaders\\" + (std::string)shaderName + ".ps";

	verName	= (char*)tempVer.c_str();
	pixName	= (char*)tempPix.c_str();

	//Initialise vertex and pixel shader
	result = InitialiseShader(device, hWnd,verName, pixName);
	if(!result) return false;

	return true;
}

void CShader::Clean()
{
	//Shutdown the shaders
	ShutdownShader();
}

bool CShader::Render(ID3D11DeviceContext* deviceContext,int indexCount,D3DXMATRIX worldMatrix,
	D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix,ID3D11ShaderResourceView** textures,
	ID3D11ShaderResourceView** specular,D3DXVECTOR3 cameraPos,CLight* light)
{
	bool result;

	//Set the shader parameters
	result = SetShaderParameters(deviceContext,worldMatrix,viewMatrix,projMatrix,textures,specular,light->GetDirection(),
				light->GetAmbientColour(),light->GetDiffuseColour(),cameraPos,light->GetSpecularColour(),light->GetSpecularPower());
	if(!result) return false;

	//Render buffers with shader
	RenderShader(deviceContext, indexCount);

	return true;
}

bool CShader::Render(ID3D11DeviceContext* deviceContext,int indexCount,D3DXMATRIX worldMatrix,
	D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix,ID3D11ShaderResourceView* textures)
{
	bool result;

	//Set the shader parameters
	result = SetShaderParameters(deviceContext,worldMatrix,viewMatrix,projMatrix,textures);
	if(!result) return false;

	//Render buffers with shader
	RenderShader(deviceContext,indexCount);

	return true;
}

bool CShader::Render(ID3D11DeviceContext* deviceContext,int indexCount,D3DXMATRIX worldMatrix,
	D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix)
{
	bool result;

	//Set the shader parameters
	result = SetShaderParameters(deviceContext,worldMatrix,viewMatrix,projMatrix);
	if(!result) return false;

	//Render buffers with shader
	RenderShader(deviceContext,indexCount);

	return true;
}

//bool CShader::Render(ID3D11DeviceContext* deviceContext,int indexCount,D3DXMATRIX worldMatrix,
//	D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix,ID3D11ShaderResourceView* textures,
//	ID3D11ShaderResourceView* reflectTexture, D3DXMATRIX reflectMatrix)
//{
//	bool result;
//
//	//Set the shader parameters
//	result = SetShaderParameters(deviceContext,worldMatrix,viewMatrix,projMatrix,textures,reflectTexture,reflectMatrix);
//	if(!result) return false;
//
//	//Render buffers with shader
//	RenderShader(deviceContext,indexCount);
//
//	return true;
//}

bool CShader::Render(ID3D11DeviceContext* deviceContext,int indexCount,D3DXMATRIX worldMatrix,D3DXMATRIX viewMatrix,
	D3DXMATRIX projMatrix,D3DXMATRIX lightViewMatrix,D3DXMATRIX lightProjMatrix,
	ID3D11ShaderResourceView** texture,ID3D11ShaderResourceView* depthMapTexture,D3DXVECTOR3 lightPos,
	D3DXVECTOR4 ambientColour,D3DXVECTOR4 diffuseColour)
{
	bool result;
	
	result = SetShaderParameters(deviceContext,worldMatrix,viewMatrix,projMatrix,lightViewMatrix,lightProjMatrix,
					texture,depthMapTexture,lightPos,ambientColour,diffuseColour);

	if(!result)
		return false;

	RenderShader(deviceContext,indexCount);

	return true;
}

bool CShader::InitialiseShader(ID3D11Device* device, HWND hWnd, LPCSTR verName, LPCSTR pixName)
{
	HRESULT result;
	ID3D10Blob* errorMessage;
	ID3D10Blob* vsBuffer;
	ID3D10Blob* psBuffer;
	D3D11_INPUT_ELEMENT_DESC polygonLayout[3];
	unsigned int numElements;
	D3D11_BUFFER_DESC matrixBufferDesc;
	D3D11_BUFFER_DESC cameraBufferDesc;
	D3D11_BUFFER_DESC lightBufferDesc;
	D3D11_BUFFER_DESC lightBufferDesc2;
	//D3D11_BUFFER_DESC reflectBufferDesc;
	D3D11_SAMPLER_DESC samplerDesc;

	//Initialise pointers
	errorMessage = 0;
	vsBuffer = 0;
	psBuffer = 0;

	//Compile the vertex shaders code
	result = D3DX11CompileFromFile(verName, NULL, NULL, "Vertex", "vs_5_0",
				D3D10_SHADER_ENABLE_STRICTNESS, 0, NULL, &vsBuffer, &errorMessage, NULL);

	if(FAILED(result))
	{
		//If shader failed
		if(errorMessage)
			OutputShaderErrorMessage(errorMessage, hWnd,verName);
		//If there is nothing in the error message
		else
			MessageBox(hWnd,verName, "Shader file is missing", MB_OK);
		return false;
	}

	// Compile the pixel shader code.
	result = D3DX11CompileFromFile(pixName, NULL, NULL, "Pixel", "ps_5_0",
				D3D10_SHADER_ENABLE_STRICTNESS, 0, NULL, &psBuffer, &errorMessage, NULL);

	if(FAILED(result))
	{
		//If shader failed
		if(errorMessage)
			OutputShaderErrorMessage(errorMessage, hWnd,pixName);
		//If there is nothing in the error message
		else
			MessageBox(hWnd,pixName, "Shader file is missing", MB_OK);
		return false;
	}

	//Create vertex shader from buffer
	result = device->CreateVertexShader(vsBuffer->GetBufferPointer(), vsBuffer->GetBufferSize(),
																		NULL, &m_VertexShader);
	if(FAILED(result)) return false;

	//Create pixel shader from buffer
	result = device->CreatePixelShader(psBuffer->GetBufferPointer(), psBuffer->GetBufferSize(),
																		NULL, &m_PixelShader);
	if(FAILED(result)) return false;

	//Setup layout of data into the shader
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "TEXCOORD";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	polygonLayout[2].SemanticName = "NORMAL";
	polygonLayout[2].SemanticIndex = 0;
	polygonLayout[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[2].InputSlot = 0;
	polygonLayout[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[2].InstanceDataStepRate = 0;

	//Get count of elements
	numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	//Create vertex input layout
	result = device->CreateInputLayout(polygonLayout, numElements, vsBuffer->GetBufferPointer(), 
										vsBuffer->GetBufferSize(), &m_Layout);
	if(FAILED(result)) return false;

	//Release buffers
	vsBuffer->Release();
	vsBuffer = 0;

	psBuffer->Release();
	psBuffer = 0;

	//Setup description of the matrix constant buffer
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBuffer);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	//Create constant buffer pointer
	result = device->CreateBuffer(&matrixBufferDesc, NULL, &m_MatrixBuffer);
	if(FAILED(result)) return false;

	//Create texture sampler description
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	result = device->CreateSamplerState(&samplerDesc, &m_SampleStateWrap);
	if(FAILED(result)) return false;

	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;

	result = device->CreateSamplerState(&samplerDesc,&m_SampleStateClamp);
	if(FAILED(result)) return false;

	//Setup description of the camera buffer
	cameraBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	cameraBufferDesc.ByteWidth = sizeof(CameraBuffer);
	cameraBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cameraBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cameraBufferDesc.MiscFlags = 0;
	cameraBufferDesc.StructureByteStride = 0;

	result = device->CreateBuffer(&cameraBufferDesc, NULL, &m_CameraBuffer);
	if(FAILED(result)) return false;

	//Create lighting stuff - ByteWidth MUST be a multiple of 16 when using D3D11_BIND_CONSTANT_BUFFER
	lightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	lightBufferDesc.ByteWidth = sizeof(LightBuffer);
	lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBufferDesc.MiscFlags = 0;
	lightBufferDesc.StructureByteStride = 0;

	result = device->CreateBuffer(&lightBufferDesc, NULL, &m_LightBuffer);
	if(FAILED(result)) return false;

	lightBufferDesc2.Usage = D3D11_USAGE_DYNAMIC;
	lightBufferDesc2.ByteWidth = sizeof(LightBuffer2);
	lightBufferDesc2.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBufferDesc2.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBufferDesc2.MiscFlags = 0;
	lightBufferDesc2.StructureByteStride = 0;

	result = device->CreateBuffer(&lightBufferDesc2,NULL,&m_LightBuffer2);
	if(FAILED(result)) return false;

	////Setup the description of the reflection dynamic constant buffer that is in the vertex shader.
	//reflectBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	//reflectBufferDesc.ByteWidth = sizeof(reflectBufferDesc);
	//reflectBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	//reflectBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	//reflectBufferDesc.MiscFlags = 0;
	//reflectBufferDesc.StructureByteStride = 0;

	//// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	//result = device->CreateBuffer(&reflectBufferDesc,NULL,&m_ReflectBuffer);
	//if(FAILED(result)) return false;

	return true;
}

void CShader::ShutdownShader()
{
	//Release  all the shader stuff
	if(m_SampleStateClamp)
	{
		m_SampleStateClamp->Release();
		m_SampleStateClamp = 0;
	}

	if(m_SampleStateWrap)
	{
		m_SampleStateWrap->Release();
		m_SampleStateWrap = 0;
	}

	if(m_MatrixBuffer)
	{
		m_MatrixBuffer->Release();
		m_MatrixBuffer = 0;
	}

	if(m_LightBuffer)
	{
		m_LightBuffer->Release();
		m_LightBuffer = 0;
	}

	if(m_CameraBuffer)
	{
		m_CameraBuffer->Release();
		m_CameraBuffer = 0;
	}

	if(m_Layout)
	{
		m_Layout->Release();
		m_Layout = 0;
	}

	if(m_PixelShader)
	{
		m_PixelShader->Release();
		m_PixelShader = 0;
	}

	if(m_VertexShader)
	{
		m_VertexShader->Release();
		m_VertexShader = 0;
	}
}

void CShader::OutputShaderErrorMessage(ID3D10Blob* errorMessage, HWND hWnd, LPCSTR filename)
{
	char* compileErrors;
	unsigned long bufferSize, i;
	std::ofstream fout;

	//Pointer to error message
	compileErrors = (char*)(errorMessage->GetBufferPointer());

	//Get length
	bufferSize = errorMessage->GetBufferSize();

	//Open a file to write to
	fout.open("shader-error.txt");

	//Write
	for(i = 0; i < bufferSize; i++)
	{
		fout<<compileErrors[i];
	}

	//Close
	fout.close();

	//Release
	errorMessage->Release();
	errorMessage = 0;

	//Popup message
	MessageBox(hWnd, "Error compiling shader. Check shader-error.txt for message", filename, MB_OK);
}

bool CShader::SetShaderParameters(ID3D11DeviceContext* deviceContext,D3DXMATRIX worldMatrix,
	D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix,ID3D11ShaderResourceView** textures,
	ID3D11ShaderResourceView** specular,D3DXVECTOR3 lightDir,D3DXVECTOR4 ambientColour,
	D3DXVECTOR4 diffuseColour,D3DXVECTOR3 cameraPos,D3DXVECTOR4 specularColour,float specularPower)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBuffer* matrixData;
	LightBuffer* lightData;
	CameraBuffer* cameraData;
	unsigned int bufferNumber;

	//Transpose matrices
	D3DXMatrixTranspose(&worldMatrix, &worldMatrix);
	D3DXMatrixTranspose(&viewMatrix, &viewMatrix);
	D3DXMatrixTranspose(&projMatrix, &projMatrix);

	//Lock the constant buffer
	result = deviceContext->Map(m_MatrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if(FAILED(result)) return false;

	//Get pointer
	matrixData = (MatrixBuffer*)mappedResource.pData;

	//Copy matricies into constant buffer
	matrixData->worldMatrix = worldMatrix;
	matrixData->viewMatrix = viewMatrix;
	matrixData->projMatrix = projMatrix;

	//Unlock constant buffer
	deviceContext->Unmap(m_MatrixBuffer, 0);

	//Set position of constant buffer
	bufferNumber = 0;

	//Set constant buffer in vertex shader
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_MatrixBuffer);

	//Lock the camera buffer
	result = deviceContext->Map(m_CameraBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(result)) return false;

	//Get a pointer to the data in the constant buffer.
	cameraData = (CameraBuffer*)mappedResource.pData;

	//Copy the camera position into the constant buffer.
	cameraData->cameraPos = cameraPos;
	cameraData->padding = 0.0f;

	//Unlock the camera buffer.
	deviceContext->Unmap(m_CameraBuffer,0);

	//Set the position of the camera constant buffer in the vertex shader.
	bufferNumber = 1;

	//Now set the camera constant buffer in the vertex shader with the updated values.
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_CameraBuffer);

	//Set shader texture
	deviceContext->PSSetShaderResources(0, 3, textures);
	if(specular)
		deviceContext->PSSetShaderResources(0, 2, specular);

	//---|Same for the lighting|---//
	//Lock the constant buffer
	result = deviceContext->Map(m_LightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if(FAILED(result)) return false;

	//Get pointer
	lightData = (LightBuffer*)mappedResource.pData;

	//Copy matricies into constant buffer
	lightData->ambientColour	= ambientColour;
	lightData->diffuseColour	= diffuseColour;
	lightData->lightDir			= lightDir;
	lightData->specularColour	= specularColour;
	lightData->specularPower	= specularPower;

	//Unlock constant buffer
	deviceContext->Unmap(m_LightBuffer, 0);

	//Set position of constant buffer in pixel shader
	bufferNumber = 0;

	//Set constant buffer in pixel shader
	deviceContext->PSSetConstantBuffers(bufferNumber, 1, &m_LightBuffer);

	return true;
}

bool CShader::SetShaderParameters(ID3D11DeviceContext* deviceContext,D3DXMATRIX worldMatrix,
	D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix,ID3D11ShaderResourceView* textures)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBuffer* matrixData;
	unsigned int bufferNumber;


	//Lock the constant buffer
	result = deviceContext->Map(m_MatrixBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(result)) return false;

	//Get pointer
	matrixData = (MatrixBuffer*)mappedResource.pData;

	//Transpose matrices
	D3DXMatrixTranspose(&worldMatrix,&worldMatrix);
	D3DXMatrixTranspose(&viewMatrix,&viewMatrix);
	D3DXMatrixTranspose(&projMatrix,&projMatrix);

	//Copy matricies into constant buffer
	matrixData->worldMatrix = worldMatrix;
	matrixData->viewMatrix = viewMatrix;
	matrixData->projMatrix = projMatrix;

	//Unlock constant buffer
	deviceContext->Unmap(m_MatrixBuffer,0);

	//Set position of constant buffer
	bufferNumber = 0;

	//Set constant buffer in vertex shader
	deviceContext->VSSetConstantBuffers(bufferNumber,1,&m_MatrixBuffer);
	//Set shader texture
	deviceContext->PSSetShaderResources(0,1,&textures);

	return true;
}

bool CShader::SetShaderParameters(ID3D11DeviceContext* deviceContext,D3DXMATRIX worldMatrix,
	D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBuffer* matrixData;
	unsigned int bufferNumber;


	//Lock the constant buffer
	result = deviceContext->Map(m_MatrixBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(result)) return false;

	//Get pointer
	matrixData = (MatrixBuffer*)mappedResource.pData;

	//Transpose matrices
	D3DXMatrixTranspose(&worldMatrix,&worldMatrix);
	D3DXMatrixTranspose(&viewMatrix,&viewMatrix);
	D3DXMatrixTranspose(&projMatrix,&projMatrix);

	//Copy matricies into constant buffer
	matrixData->worldMatrix = worldMatrix;
	matrixData->viewMatrix = viewMatrix;
	matrixData->projMatrix = projMatrix;

	//Unlock constant buffer
	deviceContext->Unmap(m_MatrixBuffer,0);

	//Set position of constant buffer
	bufferNumber = 0;

	//Set constant buffer in vertex shader
	deviceContext->VSSetConstantBuffers(bufferNumber,1,&m_MatrixBuffer);

	return true;
}

//bool CShader::SetShaderParameters(ID3D11DeviceContext* deviceContext,D3DXMATRIX worldMatrix,
//	D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix,ID3D11ShaderResourceView* textures,
//	ID3D11ShaderResourceView* reflectTexture,D3DXMATRIX reflectMatrix)
//{
//	HRESULT result;
//	D3D11_MAPPED_SUBRESOURCE mappedResource;
//	MatrixBuffer* matrixData;
//	unsigned int bufferNumber;
//	ReflectBuffer* reflectData;
//
//	//Transpose matrices
//	D3DXMatrixTranspose(&worldMatrix,&worldMatrix);
//	D3DXMatrixTranspose(&viewMatrix,&viewMatrix);
//	D3DXMatrixTranspose(&projMatrix,&projMatrix);
//	D3DXMatrixTranspose(&reflectMatrix,&reflectMatrix);
//
//	//Lock the constant buffer
//	result = deviceContext->Map(m_MatrixBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
//	if(FAILED(result)) return false;
//
//	//Get pointer
//	matrixData = (MatrixBuffer*)mappedResource.pData;
//
//	//Copy matricies into constant buffer
//	matrixData->worldMatrix = worldMatrix;
//	matrixData->viewMatrix = viewMatrix;
//	matrixData->projMatrix = projMatrix;
//
//	//Unlock constant buffer
//	deviceContext->Unmap(m_MatrixBuffer,0);
//
//	//Set position of constant buffer
//	bufferNumber = 0;
//
//	//Set constant buffer in vertex shader
//	deviceContext->VSSetConstantBuffers(bufferNumber,1,&m_MatrixBuffer);
//
//	//Same for the reflect buffer
//	result = deviceContext->Map(m_ReflectBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
//	if(FAILED(result)) return false;
//
//	reflectData = (ReflectBuffer*)mappedResource.pData;
//
//	reflectData->reflectMatrix = reflectMatrix;
//
//	deviceContext->Unmap(m_ReflectBuffer,0);
//
//	bufferNumber = 1;
//
//	deviceContext->VSSetConstantBuffers(bufferNumber,1,&m_ReflectBuffer);
//	//Set shader texture
//	deviceContext->PSSetShaderResources(0,1,&textures);
//
//	deviceContext->PSSetShaderResources(1,1,&reflectTexture);
//
//	return true;
//}

bool CShader::SetShaderParameters(ID3D11DeviceContext* deviceContext,D3DXMATRIX worldMatrix,D3DXMATRIX viewMatrix,
	D3DXMATRIX projMatrix,D3DXMATRIX lightViewMatrix,D3DXMATRIX lightProjMatrix,ID3D11ShaderResourceView** texture,
	ID3D11ShaderResourceView* depthMapTexture,D3DXVECTOR3 lightPos,D3DXVECTOR4 ambientColour,D3DXVECTOR4 diffuseColour)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	unsigned int bufferNumber;
	MatrixBuffer* matrixBuffer;
	LightBuffer* lightBuffer;
	LightBuffer2* lightBuffer2;


	// Transpose the matrices to prepare them for the shader.
	D3DXMatrixTranspose(&worldMatrix,&worldMatrix);
	D3DXMatrixTranspose(&viewMatrix,&viewMatrix);
	D3DXMatrixTranspose(&projMatrix,&projMatrix);

	D3DXMatrixTranspose(&lightViewMatrix,&lightViewMatrix);
	D3DXMatrixTranspose(&lightProjMatrix,&lightProjMatrix);

	// Lock the constant buffer so it can be written to.
	result = deviceContext->Map(m_MatrixBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(result)) return false;

	// Get a pointer to the data in the constant buffer.
	matrixBuffer = (MatrixBuffer*)mappedResource.pData;

	// Copy the matrices into the constant buffer.
	matrixBuffer->worldMatrix = worldMatrix;
	matrixBuffer->viewMatrix = viewMatrix;
	matrixBuffer->projMatrix = projMatrix;

	matrixBuffer->lightViewMatrix = lightViewMatrix;
	matrixBuffer->lightProjMatrix = lightProjMatrix;

	// Unlock the constant buffer.
	deviceContext->Unmap(m_MatrixBuffer,0);

	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;

	// Now set the constant buffer in the vertex shader with the updated values.
	deviceContext->VSSetConstantBuffers(bufferNumber,1,&m_MatrixBuffer);

	// Set shader texture resource in the pixel shader.
	deviceContext->PSSetShaderResources(0,1,texture);

	deviceContext->PSSetShaderResources(1,1,&depthMapTexture);

	// Lock the light constant buffer so it can be written to.
	result = deviceContext->Map(m_LightBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(result)) return false;

	// Get a pointer to the data in the constant buffer.
	lightBuffer = (LightBuffer*)mappedResource.pData;

	// Copy the lighting variables into the constant buffer.
	lightBuffer->ambientColour = ambientColour;
	lightBuffer->diffuseColour = diffuseColour;

	// Unlock the constant buffer.
	deviceContext->Unmap(m_LightBuffer,0);

	// Set the position of the light constant buffer in the pixel shader.
	bufferNumber = 0;

	// Finally set the light constant buffer in the pixel shader with the updated values.
	deviceContext->PSSetConstantBuffers(bufferNumber,1,&m_LightBuffer);

	// Lock the second light constant buffer so it can be written to.
	result = deviceContext->Map(m_LightBuffer2,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(result)) return false;

	// Get a pointer to the data in the constant buffer.
	lightBuffer2 = (LightBuffer2*)mappedResource.pData;

	// Copy the lighting variables into the constant buffer.
	lightBuffer2->lightPos = lightPos;
	lightBuffer2->padding = 0.0f;

	// Unlock the constant buffer.
	deviceContext->Unmap(m_LightBuffer2,0);

	// Set the position of the light constant buffer in the vertex shader.
	bufferNumber = 1;

	// Finally set the light constant buffer in the pixel shader with the updated values.
	deviceContext->VSSetConstantBuffers(bufferNumber,1,&m_LightBuffer2);

	return true;
}

void CShader::RenderShader(ID3D11DeviceContext* deviceContext, int indexCount)
{
	//Set vertex input layout
	deviceContext->IASetInputLayout(m_Layout);

	//Set vertex and pixel shaders
	deviceContext->VSSetShader(m_VertexShader, NULL, 0);
	deviceContext->PSSetShader(m_PixelShader, NULL, 0);

	//Set sampler state
	deviceContext->PSSetSamplers(0,1,&m_SampleStateClamp);
	deviceContext->PSSetSamplers(1,1,&m_SampleStateWrap);

	//Render triangle
	deviceContext->DrawIndexed(indexCount, 0, 0);
}