#include "Text.h"

CText::CText()
{
	m_Font		 = 0;
	m_FontShader = 0;

	m_Sentence1  = 0;
	m_Sentence2	 = 0;
}

bool CText::Initialise(ID3D11Device* device,ID3D11DeviceContext* deviceContext,HWND hWnd,int screenWidth,
																int screenHeight,D3DXMATRIX baseViewMatrix)
{
	bool result;

	m_ScreenWidth = screenWidth;
	m_ScreenHeight = screenHeight;
	m_BaseViewMatrix = baseViewMatrix;

	m_Font = new CFontHandler;
	if(!m_Font) return false;
	
	result = m_Font->Initialise(device,"..//Resources//FontData.txt","..//Resources//Font.dds");
	if(!result)
	{
		MessageBox(hWnd,"Could not initialize the font object.","Error",MB_OK);
		return false;
	}

	m_FontShader = new CFontShader;
	if(!m_FontShader) return false;

	result = m_FontShader->Initialise(device,hWnd);
	if(!result)
	{
		MessageBox(hWnd,"Could not initialize the font shader object.","Error",MB_OK);
		return false;
	}

	result = InitialiseSentence(device,&m_Sentence1,16);
	if(!result) return false;

	result = UpdateSentence(deviceContext,m_Sentence1,"Hello",100,100,1.0f,1.0f,1.0f);
	if(!result) return false;

	result = InitialiseSentence(device,&m_Sentence2,16);
	if(!result) return false;

	result = UpdateSentence(deviceContext,m_Sentence2,"Goodbye",100,600,1.0f,1.0f,0.0f);
	if(!result) return false;

	return true;
}

void CText::Clean()
{
	ReleaseSentence(&m_Sentence1);
	ReleaseSentence(&m_Sentence2);

	if(m_FontShader)
	{
		m_FontShader->Clean();
		delete m_FontShader;
		m_FontShader = 0;
	}

	if(m_Font)
	{
		m_Font->Clean();
		delete m_Font;
		m_Font = 0;
	}
}

bool CText::Render(ID3D11DeviceContext* deviceContext,D3DXMATRIX worldMatrix,D3DXMATRIX orthoMatrix)
{
	bool result;

	result = RenderSentence(deviceContext,m_Sentence1,worldMatrix,orthoMatrix);
	if(!result) return false;

	result = RenderSentence(deviceContext,m_Sentence2,worldMatrix,orthoMatrix);
	if(!result) return false;

	return true;
}

bool CText::SetMousePosition(ID3D11DeviceContext* deviceContext,int mouseX,int mouseY)
{
	bool result;
	char tempString[16];
	char mouseString[16];

	_itoa_s(mouseX,tempString,10);

	strcpy_s(mouseString,"Mouse X: ");
	strcat_s(mouseString,tempString);

	result = UpdateSentence(deviceContext,m_Sentence1,mouseString,20,20,1.0f,1.0f,1.0f);
	if(!result) return false;

	_itoa_s(mouseY,tempString,10);

	strcpy_s(mouseString,"Mouse Y: ");
	strcat_s(mouseString,tempString);

	result = UpdateSentence(deviceContext,m_Sentence2,mouseString,20,40,1.0f,1.0f,1.0f);
	if(!result) return false;

	return true;
}

bool CText::InitialiseSentence(ID3D11Device* device,Sentence** sentence,int maxLength)
{
	Vertex* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc,indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData,indexData;
	HRESULT hr;

	*sentence = new Sentence;
	if(!*sentence) return false;

	(*sentence)->vertexBuffer	= 0;
	(*sentence)->indexBuffer	= 0;

	(*sentence)->maxLength = maxLength;
	(*sentence)->vertexCount = 6 * maxLength;
	(*sentence)->indexCount = (*sentence)->vertexCount;

	vertices = new Vertex[(*sentence)->vertexCount];
	if(!vertices) return false;

	indices = new unsigned long[(*sentence)->indexCount];
	if(!indices) return false;

	memset(vertices,0,(sizeof(Vertex) * (*sentence)->vertexCount));

	for(int i = 0; i<(*sentence)->indexCount; i++)
		indices[i] = i;

	vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth = sizeof(Vertex) * (*sentence)->vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	hr = device->CreateBuffer(&vertexBufferDesc,&vertexData,&(*sentence)->vertexBuffer);
	if(FAILED(hr)) return false;

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * (*sentence)->indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	hr = device->CreateBuffer(&indexBufferDesc,&indexData,&(*sentence)->indexBuffer);
	if(FAILED(hr)) return false;

	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;

	return true;
}

bool CText::UpdateSentence(ID3D11DeviceContext* deviceContext,Sentence* sentence,char* text,int posX,int posY,
							float r,float g,float b)
{
	int numCharacters;
	Vertex* vertices;
	float drawX,drawY;
	HRESULT hr;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	Vertex* verticesData;

	sentence->r = r;
	sentence->g = g;
	sentence->b = b;

	numCharacters = (int)strlen(text);

	if(numCharacters > sentence->maxLength) return false;

	vertices = new Vertex[sentence->vertexCount];
	if(!vertices) return false;

	memset(vertices,0,(sizeof(Vertex) * sentence->vertexCount));

	drawX = (float)(((m_ScreenWidth	 / 2) * -1) + posX);
	drawY = (float)((m_ScreenHeight / 2) - posY);

	m_Font->BuildVertexArray((void*)vertices,text,drawX,drawY);

	hr = deviceContext->Map(sentence->vertexBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(hr)) return false;

	verticesData = (Vertex*)mappedResource.pData;

	memcpy(verticesData,(void*)vertices,(sizeof(Vertex) * sentence->vertexCount));

	deviceContext->Unmap(sentence->vertexBuffer,0);

	delete[] vertices;
	vertices = 0;

	return true;
}

void CText::ReleaseSentence(Sentence** sentence)
{
	if(*sentence)
	{
		if((*sentence)->vertexBuffer)
		{
			(*sentence)->vertexBuffer->Release();
			(*sentence)->vertexBuffer = 0;
		}

		if((*sentence)->indexBuffer)
		{
			(*sentence)->indexBuffer->Release();
			(*sentence)->indexBuffer = 0;
		}

		delete *sentence;
		*sentence = 0;
	}
}

bool CText::RenderSentence(ID3D11DeviceContext* deviceContext,Sentence* sentence,D3DXMATRIX worldMatrix,
																					D3DXMATRIX orthoMatrix)
{
	unsigned int stride,offset;
	D3DXVECTOR4 pixelColor;
	bool result;

	stride = sizeof(Vertex);
	offset = 0;

	deviceContext->IASetVertexBuffers(0,1,&sentence->vertexBuffer,&stride,&offset);

	deviceContext->IASetIndexBuffer(sentence->indexBuffer,DXGI_FORMAT_R32_UINT,0);

	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	pixelColor = D3DXVECTOR4(sentence->r,sentence->g,sentence->b,1.0f);

	result = m_FontShader->Render(deviceContext,sentence->indexCount,worldMatrix,m_BaseViewMatrix,
																orthoMatrix,m_Font->GetTexture(),pixelColor);
	if(!result) return false;

	return true;
}