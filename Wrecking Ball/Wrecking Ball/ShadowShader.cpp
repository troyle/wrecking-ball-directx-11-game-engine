#include "ShadowShader.h"

CShadowShader::CShadowShader()
{
	m_VertexShader		= 0;
	m_PixelShader		= 0;
	m_Layout			= 0;
	m_SampleStateWrap	= 0;
	m_SampleStateClamp	= 0;
	m_MatrixBuffer		= 0;
	m_LightBuffer		= 0;
	m_LightBuffer2		= 0;
}

bool CShadowShader::InitialiseShader(ID3D11Device* device,HWND hwnd,LPCSTR verName,LPCSTR pixName)
{
	HRESULT result;
	ID3D10Blob* errorMessage;
	ID3D10Blob* vertexShaderBuffer;
	ID3D10Blob* pixelShaderBuffer;
	D3D11_INPUT_ELEMENT_DESC polygonLayout[3];
	unsigned int numElements;
	D3D11_SAMPLER_DESC samplerDesc;
	D3D11_BUFFER_DESC matrixBufferDesc;
	D3D11_BUFFER_DESC lightBufferDesc;
	D3D11_BUFFER_DESC lightBufferDesc2;


	errorMessage = 0;
	vertexShaderBuffer = 0;
	pixelShaderBuffer = 0;

	result = D3DX11CompileFromFile(verName,NULL,NULL,"Vertex","vs_5_0",D3D10_SHADER_ENABLE_STRICTNESS,0,NULL,
		&vertexShaderBuffer,&errorMessage,NULL);
	if(FAILED(result))
	{
		if(errorMessage)
			OutputShaderErrorMessage(errorMessage,hwnd,verName);
		else
			MessageBox(hwnd,verName,"Missing Shader File",MB_OK);

		return false;
	}

	result = D3DX11CompileFromFile(pixName,NULL,NULL,"Pixel","ps_5_0",D3D10_SHADER_ENABLE_STRICTNESS,0,NULL,
		&pixelShaderBuffer,&errorMessage,NULL);
	if(FAILED(result))
	{
		if(errorMessage)
			OutputShaderErrorMessage(errorMessage,hwnd,pixName);
		else
			MessageBox(hwnd,pixName,"Missing Shader File",MB_OK);

		return false;
	}

	result = device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(),vertexShaderBuffer->GetBufferSize(),NULL,&m_VertexShader);
	if(FAILED(result)) return false;

	result = device->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(),pixelShaderBuffer->GetBufferSize(),NULL,&m_PixelShader);
	if(FAILED(result)) return false;

	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "TEXCOORD";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	polygonLayout[2].SemanticName = "NORMAL";
	polygonLayout[2].SemanticIndex = 0;
	polygonLayout[2].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[2].InputSlot = 0;
	polygonLayout[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[2].InstanceDataStepRate = 0;

	numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	result = device->CreateInputLayout(polygonLayout,numElements,vertexShaderBuffer->GetBufferPointer(),vertexShaderBuffer->GetBufferSize(),
		&m_Layout);
	if(FAILED(result)) return false;

	vertexShaderBuffer->Release();
	vertexShaderBuffer = 0;

	pixelShaderBuffer->Release();
	pixelShaderBuffer = 0;

	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	result = device->CreateSamplerState(&samplerDesc,&m_SampleStateWrap);
	if(FAILED(result)) return false;

	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;

	result = device->CreateSamplerState(&samplerDesc,&m_SampleStateClamp);
	if(FAILED(result)) return false;

	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBuffer);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	result = device->CreateBuffer(&matrixBufferDesc,NULL,&m_MatrixBuffer);
	if(FAILED(result)) return false;

	lightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	lightBufferDesc.ByteWidth = sizeof(LightBuffer);
	lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBufferDesc.MiscFlags = 0;
	lightBufferDesc.StructureByteStride = 0;

	result = device->CreateBuffer(&lightBufferDesc,NULL,&m_LightBuffer);
	if(FAILED(result)) return false;

	lightBufferDesc2.Usage = D3D11_USAGE_DYNAMIC;
	lightBufferDesc2.ByteWidth = sizeof(LightBuffer2);
	lightBufferDesc2.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBufferDesc2.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBufferDesc2.MiscFlags = 0;
	lightBufferDesc2.StructureByteStride = 0;

	result = device->CreateBuffer(&lightBufferDesc2,NULL,&m_LightBuffer2);
	if(FAILED(result)) return false;

	return true;
}

void CShadowShader::ShutdownShader()
{
	if(m_LightBuffer)
	{
		m_LightBuffer->Release();
		m_LightBuffer = 0;
	}

	if(m_LightBuffer2)
	{
		m_LightBuffer2->Release();
		m_LightBuffer2 = 0;
	}

	if(m_MatrixBuffer)
	{
		m_MatrixBuffer->Release();
		m_MatrixBuffer = 0;
	}

	if(m_SampleStateWrap)
	{
		m_SampleStateWrap->Release();
		m_SampleStateWrap = 0;
	}

	if(m_SampleStateClamp)
	{
		m_SampleStateClamp->Release();
		m_SampleStateClamp = 0;
	}

	if(m_Layout)
	{
		m_Layout->Release();
		m_Layout = 0;
	}

	if(m_PixelShader)
	{
		m_PixelShader->Release();
		m_PixelShader = 0;
	}

	if(m_VertexShader)
	{
		m_VertexShader->Release();
		m_VertexShader = 0;
	}
}

void CShadowShader::OutputShaderErrorMessage(ID3D10Blob* errorMessage,HWND hWnd,LPCSTR filename)
{
	char* compileErrors;
	unsigned long bufferSize,i;
	std::ofstream fout;

	//Pointer to error message
	compileErrors = (char*)(errorMessage->GetBufferPointer());

	//Get length
	bufferSize = errorMessage->GetBufferSize();

	//Open a file to write to
	fout.open("shader-error.txt");

	//Write
	for(i = 0; i < bufferSize; i++)
	{
		fout<<compileErrors[i];
	}

	//Close
	fout.close();

	//Release
	errorMessage->Release();
	errorMessage = 0;

	//Popup message
	MessageBox(hWnd,"Error compiling shader. Check shader-error.txt for message",filename,MB_OK);
}

bool CShadowShader::SetShaderParameters(ID3D11DeviceContext* deviceContext,D3DXMATRIX worldMatrix,
	D3DXMATRIX viewMatrix,D3DXMATRIX projMatrix,D3DXMATRIX lightViewMatrix,D3DXMATRIX lightProjMatrix,
	ID3D11ShaderResourceView** texture,ID3D11ShaderResourceView* depthMapTexture,D3DXVECTOR3 lightPos,
	D3DXVECTOR4 ambientColour,D3DXVECTOR4 diffuseColour)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	unsigned int bufferNumber;
	MatrixBuffer* matrixBuffer;
	LightBuffer* lightBuffer;
	LightBuffer2* lightBuffer2;

	D3DXMatrixTranspose(&worldMatrix,&worldMatrix);
	D3DXMatrixTranspose(&viewMatrix,&viewMatrix);
	D3DXMatrixTranspose(&projMatrix,&projMatrix);
	D3DXMatrixTranspose(&lightViewMatrix,&lightViewMatrix);
	D3DXMatrixTranspose(&lightProjMatrix,&lightProjMatrix);

	result = deviceContext->Map(m_MatrixBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(result)) return false;

	matrixBuffer = (MatrixBuffer*)mappedResource.pData;

	matrixBuffer->worldMatrix = worldMatrix;
	matrixBuffer->viewMatrix = viewMatrix;
	matrixBuffer->projMatrix = projMatrix;
	matrixBuffer->lightViewMatrix = lightViewMatrix;
	matrixBuffer->lightProjMatrix = lightProjMatrix;

	deviceContext->Unmap(m_MatrixBuffer,0);

	bufferNumber = 0;

	deviceContext->VSSetConstantBuffers(bufferNumber,1,&m_MatrixBuffer);

	deviceContext->PSSetShaderResources(0,1,texture);
	deviceContext->PSSetShaderResources(1,1,&depthMapTexture);

	result = deviceContext->Map(m_LightBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(result)) return false;

	lightBuffer = (LightBuffer*)mappedResource.pData;

	lightBuffer->ambientColour = ambientColour;
	lightBuffer->diffuseColour = diffuseColour;

	deviceContext->Unmap(m_LightBuffer,0);

	bufferNumber = 0;

	deviceContext->PSSetConstantBuffers(bufferNumber,1,&m_LightBuffer);

	result = deviceContext->Map(m_LightBuffer2,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(result))return false;

	lightBuffer2 = (LightBuffer2*)mappedResource.pData;

	lightBuffer2->lightPos = lightPos;
	lightBuffer2->padding = 0.0f;

	deviceContext->Unmap(m_LightBuffer2,0);

	bufferNumber = 1;

	deviceContext->VSSetConstantBuffers(bufferNumber,1,&m_LightBuffer2);

	return true;
}

void CShadowShader::RenderShader(ID3D11DeviceContext* deviceContext,int indexCount)
{
	deviceContext->IASetInputLayout(m_Layout);

	deviceContext->VSSetShader(m_VertexShader,NULL,0);
	deviceContext->PSSetShader(m_PixelShader,NULL,0);

	deviceContext->PSSetSamplers(0,1,&m_SampleStateClamp);
	deviceContext->PSSetSamplers(1,1,&m_SampleStateWrap);

	deviceContext->DrawIndexed(indexCount,0,0);

	return;
}

bool CShadowShader::Initialise(ID3D11Device* device,HWND hWnd,LPCSTR shaderName)
{
	bool result;
	std::string tempVer,tempPix;
	char* verName;
	char* pixName;

	tempVer	= "..\\Shaders\\" + (std::string)shaderName + ".vs";
	tempPix	= "..\\Shaders\\" + (std::string)shaderName + ".ps";

	verName	= (char*)tempVer.c_str();
	pixName	= (char*)tempPix.c_str();

	//Initialise vertex and pixel shader
	result = InitialiseShader(device,hWnd,verName,pixName);
	if(!result) return false;

	return true;
}

void CShadowShader::Shutdown()
{
	ShutdownShader();
}

bool CShadowShader::Render(ID3D11DeviceContext* deviceContext,int indexCount,D3DXMATRIX worldMatrix,D3DXMATRIX viewMatrix,
	D3DXMATRIX projectionMatrix,D3DXMATRIX lightViewMatrix,D3DXMATRIX lightProjectionMatrix,
	ID3D11ShaderResourceView** texture,ID3D11ShaderResourceView* depthMapTexture,D3DXVECTOR3 lightPosition,
	D3DXVECTOR4 ambientColor,D3DXVECTOR4 diffuseColor)
{
	bool result;

	result = SetShaderParameters(deviceContext,worldMatrix,viewMatrix,projectionMatrix,lightViewMatrix,lightProjectionMatrix,texture,
		depthMapTexture,lightPosition,ambientColor,diffuseColor);
	if(!result) return false;

	RenderShader(deviceContext,indexCount);

	return true;
}