#include "FontHandler.h"

CFontHandler::CFontHandler()
{
	m_Font		= 0;
	m_Texture	= 0;
}

bool CFontHandler::Initialise(ID3D11Device*device, char* fontFilename,LPSTR textureFilename)
{
	bool result;

	result = LoadFontData(fontFilename);
	if(!result) return false;

	result = LoadTexture(device,textureFilename);
	if(!result) return false;

	return true;
}

void CFontHandler::Clean()
{
	ReleaseTexture();

	ReleaseFontData();
}

bool CFontHandler::LoadFontData(char* filename)
{
	std::ifstream file;
	char temp;

	//Font spacing buffer
	m_Font = new Font[95];
	if(!m_Font) return false;

	//Read font size
	file.open(filename);
	if(file.fail()) return false;

	for(int i = 0; i<95; i++)
	{
		file.get(temp);
		while(temp != ' ') file.get(temp);
		file.get(temp);
		while(temp != ' ') file.get(temp);

		file>>m_Font[i].left;
		file>>m_Font[i].right;
		file>>m_Font[i].size;
	}
	file.close();

	return true;
}

void CFontHandler::ReleaseFontData()
{
	if(m_Font)
	{
		delete []m_Font;
		m_Font = 0;
	}
}

bool CFontHandler::LoadTexture(ID3D11Device* device,LPSTR filename)
{
	bool result;

	m_Texture = new CTexture;
	if(!m_Texture) return false;

	result = m_Texture->LoadTexture(device, filename);
	if(!result) return false;

	return true;
}

void CFontHandler::ReleaseTexture()
{
	if(m_Texture)
	{
		m_Texture->Clean();
		delete m_Texture;
		m_Texture = 0;
	}
}

void CFontHandler::BuildVertexArray(void* vertices,char* text,float drawX,float drawY)
{
	Vertex* vertexData;
	int numCharacters, character, index = 0;

	//Persuade input vertices into Vertex structure
	vertexData = (Vertex*)vertices;

	numCharacters = (int)strlen(text);
	index = 0;

	for(int i = 0; i<numCharacters; i++)
	{
		character = ((int)text[i]) - 32;

		if(character == 0)
		{
			drawX = drawX + 3.0f;
		}
		else
		{
			vertexData[index].pos	= D3DXVECTOR3(drawX,drawY,0.0f);
			vertexData[index].UV	= D3DXVECTOR2(m_Font[character].left,0.0f);
			index++;

			vertexData[index].pos	= D3DXVECTOR3((drawX + m_Font[character].size),(drawY - 16),0.0f);
			vertexData[index].UV	= D3DXVECTOR2(m_Font[character].right,1.0f);
			index++;

			vertexData[index].pos	= D3DXVECTOR3(drawX,(drawY - 16),0.0f);
			vertexData[index].UV	= D3DXVECTOR2(m_Font[character].left,1.0f);
			index++;

			vertexData[index].pos	= D3DXVECTOR3(drawX,drawY,0.0f);
			vertexData[index].UV	= D3DXVECTOR2(m_Font[character].left,0.0f);
			index++;

			vertexData[index].pos	= D3DXVECTOR3(drawX + m_Font[character].size,drawY,0.0f);
			vertexData[index].UV	= D3DXVECTOR2(m_Font[character].right,0.0f);
			index++;

			vertexData[index].pos	= D3DXVECTOR3((drawX + m_Font[character].size),(drawY - 16),0.0f);
			vertexData[index].UV	= D3DXVECTOR2(m_Font[character].right,1.0f);
			index++;

			drawX = drawX + m_Font[character].size + 1.0f;
		}
	}
}