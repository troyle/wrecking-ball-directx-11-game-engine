#include "RenderToTexture.h"

CToTexture::CToTexture()
{
	m_RenderTargetTexture = 0;
	m_RenderTargetView = 0;
	m_ShaderResourceView = 0;
	m_DepthStencilBuffer = 0;
	m_DepthStencilView = 0;
}

bool CToTexture::Initialise(ID3D11Device* device,int width,int height,float farClip,float nearClip)
{
	D3D11_TEXTURE2D_DESC textureDesc;
	HRESULT result;
	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;
	D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
	D3D11_TEXTURE2D_DESC depthBufferDesc;
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;


	ZeroMemory(&textureDesc,sizeof(textureDesc));

	textureDesc.Width = width;
	textureDesc.Height = height;
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	result = device->CreateTexture2D(&textureDesc,NULL,&m_RenderTargetTexture);
	if(FAILED(result))	return false;

	renderTargetViewDesc.Format = textureDesc.Format;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	renderTargetViewDesc.Texture2D.MipSlice = 0;

	result = device->CreateRenderTargetView(m_RenderTargetTexture,&renderTargetViewDesc,&m_RenderTargetView);
	if(FAILED(result)) return false;

	shaderResourceViewDesc.Format = textureDesc.Format;
	shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
	shaderResourceViewDesc.Texture2D.MipLevels = 1;

	result = device->CreateShaderResourceView(m_RenderTargetTexture,&shaderResourceViewDesc,&m_ShaderResourceView);
	if(FAILED(result))	return false;

	ZeroMemory(&depthBufferDesc,sizeof(depthBufferDesc));

	depthBufferDesc.Width = width;
	depthBufferDesc.Height = height;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;
	depthBufferDesc.SampleDesc.Quality = 0;
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	result = device->CreateTexture2D(&depthBufferDesc,NULL,&m_DepthStencilBuffer);
	if(FAILED(result)) return false;

	ZeroMemory(&depthStencilViewDesc,sizeof(depthStencilViewDesc));

	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	result = device->CreateDepthStencilView(m_DepthStencilBuffer,&depthStencilViewDesc,&m_DepthStencilView);
	if(FAILED(result)) return false;

	m_Viewport.Width = (float)width;
	m_Viewport.Height = (float)height;
	m_Viewport.MinDepth = 0.0f;
	m_Viewport.MaxDepth = 1.0f;
	m_Viewport.TopLeftX = 0.0f;
	m_Viewport.TopLeftY = 0.0f;

	D3DXMatrixPerspectiveFovLH(&m_ProjMatrix,((float)D3DX_PI / 4.0f),((float)width / (float)height),nearClip,farClip);

	D3DXMatrixOrthoLH(&m_OrthoMatrix,(float)width,(float)height,nearClip,farClip);

	return true;
}

void CToTexture::Shutdown()
{
	if(m_RenderTargetTexture)
	{
		m_RenderTargetTexture->Release();
		m_RenderTargetTexture = 0;
	}

	if(m_RenderTargetView)
	{
		m_RenderTargetView->Release();
		m_RenderTargetView = 0;
	}

	if(m_ShaderResourceView)
	{
		m_ShaderResourceView->Release();
		m_ShaderResourceView = 0;
	}

	if(m_DepthStencilView)
	{
		m_DepthStencilView->Release();
		m_DepthStencilView = 0;
	}

	if(m_DepthStencilBuffer)
	{
		m_DepthStencilBuffer->Release();
		m_DepthStencilBuffer = 0;
	}
}

void CToTexture::SetRenderTarget(ID3D11DeviceContext* deviceContext)
{
	deviceContext->OMSetRenderTargets(1,&m_RenderTargetView,m_DepthStencilView);

	deviceContext->RSSetViewports(1,&m_Viewport);
}

void CToTexture::ClearRenderTarget(ID3D11DeviceContext* deviceContext,float r,float g,float b,float a)
{
	float colour[4] = {r,g,b,a};

	deviceContext->ClearRenderTargetView(m_RenderTargetView,colour);
	deviceContext->ClearDepthStencilView(m_DepthStencilView,D3D11_CLEAR_DEPTH,1.0f,0);
}

ID3D11ShaderResourceView* CToTexture::GetShaderResourceView()
{
	return m_ShaderResourceView;
}

void CToTexture::GetProjMatrix(D3DXMATRIX& projMatrix)
{
	projMatrix = m_ProjMatrix;
}

void CToTexture::GetOrthoMatrix(D3DXMATRIX& orthoMatrix)
{
	orthoMatrix = m_OrthoMatrix;
}