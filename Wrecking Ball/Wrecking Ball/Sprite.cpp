#include "Sprite.h"

CSprite::CSprite()
{
	m_VertexBuffer	= 0;
	m_IndexBuffer	= 0;
	m_Texture		= 0;
}

bool CSprite::Initialise(ID3D11Device* device,int screenWidth,int screenHeight,LPSTR textureFile,
															int bitmapWidth, int bitmapHeight)
{
	bool result;

	m_ScreenWidth	= screenWidth;
	m_ScreenHeight	= screenHeight;

	m_BitmapWidth	= bitmapWidth;
	m_BitmapHeight	= bitmapHeight;

	m_PrevPosX = m_PrevPosY = -1;

	//Initialise vertex and index buffer
	result = InitialiseBuffers(device);
	if(!result) return false;

	//Load texture
	if(textureFile != NULL)
	{
		result = LoadTexture(device,textureFile);
		if(!m_Texture) return false;
	}

	return true;
}

void CSprite::Clean()
{
	ReleaseTexture();

	ReleaseBuffers();
}

bool CSprite::Render(ID3D11DeviceContext* deviceContext, int posX, int posY)
{
	bool result;

	//Rebuild the dynamic vertex buffer
	result = UpdateBuffers(deviceContext, posX, posY);
	if(!result) return false;

	//Put buffers into the pipeline
	RenderBuffers(deviceContext);

	return true;
}

bool CSprite::InitialiseBuffers(ID3D11Device* device)
{
	Vertex* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc,indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData,indexData;
	HRESULT hr;

	//Set number of vertices for square that holds the bitmap
	m_VertexCount = m_IndexCount = 6;
	
	//Vertex array
	vertices = new Vertex[m_VertexCount];
	if(!vertices) return false;

	//Index array
	indices = new unsigned long[m_IndexCount];
	if(!indices) return false;

	//Initialise to zeros
	memset(vertices, 0, (sizeof(Vertex) * m_VertexCount));

	//Load data
	for(int i=0; i<m_IndexCount; i++)
		indices[i] = i;

	//Setup description of the static vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth = sizeof(Vertex) * m_VertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	//Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	//Create vertex buffer
	hr = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_VertexBuffer);
	if(FAILED(hr)) return false;

	//Setup description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_IndexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	//Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	//Create index buffer.
	hr = device->CreateBuffer(&indexBufferDesc,&indexData,&m_IndexBuffer);
	if(FAILED(hr)) return false;

	//Release the arrays
	delete[] vertices;
	vertices = 0;

	delete[] indices;
	indices = 0;

	return true;
}

void CSprite::ReleaseBuffers()
{
	if(m_IndexBuffer)
	{
		m_IndexBuffer->Release();
		m_IndexBuffer = 0;
	}

	if(m_VertexBuffer)
	{
		m_VertexBuffer->Release();
		m_VertexBuffer = 0;
	}
}

bool CSprite::UpdateBuffers(ID3D11DeviceContext* deviceContext, int posX, int posY)
{
	float left, right, top, bottom;
	Vertex* vertices, *verticesData;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	HRESULT hr;

	//If no change, dont update
	if((posX == m_PrevPosX) && (posY == m_PrevPosY)) return true;

	m_PrevPosX = posX;
	m_PrevPosY = posY;

	//Calculate screen coords for each side of the bitmap
	left	= (float)((m_ScreenWidth/2) * -1) + (float)posX;
	right	= left + (float)m_BitmapWidth;
	top		= (float)(m_ScreenHeight/2) - (float)posY;
	bottom	= top - (float)m_BitmapHeight;

	vertices = new Vertex[m_VertexCount];
	if(!vertices) return false;

	//Load vertex array with data
	vertices[0].pos = D3DXVECTOR3(left,top,0.0f);
	vertices[0].UV	= D3DXVECTOR2(0.0f,0.0f);

	vertices[1].pos = D3DXVECTOR3(right,bottom,0.0f);
	vertices[1].UV	= D3DXVECTOR2(1.0f,1.0f);

	vertices[2].pos = D3DXVECTOR3(left,bottom,0.0f);
	vertices[2].UV	= D3DXVECTOR2(0.0f,1.0f);

	vertices[3].pos = D3DXVECTOR3(left,top,0.0f);
	vertices[3].UV	= D3DXVECTOR2(0.0f,0.0f);

	vertices[4].pos = D3DXVECTOR3(right,top,0.0f);
	vertices[4].UV	= D3DXVECTOR2(1.0f,0.0f);

	vertices[5].pos = D3DXVECTOR3(right,bottom,0.0f);
	vertices[5].UV	= D3DXVECTOR2(1.0f,1.0f);

	//Lock vertx buffer
	hr = deviceContext->Map(m_VertexBuffer,0,D3D11_MAP_WRITE_DISCARD,0,&mappedResource);
	if(FAILED(hr)) return false;

	//Get pointer to the vertex buffer data
	verticesData = (Vertex*)mappedResource.pData;

	memcpy(verticesData,(void*)vertices,(sizeof(Vertex) * m_VertexCount));

	//Unlock vertex buffer.
	deviceContext->Unmap(m_VertexBuffer,0);

	delete[] vertices;
	vertices = 0;

	return true;
}

void CSprite::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride, offset;

	stride = sizeof(Vertex);
	offset = 0;

	//Set the vertex buffer to active
	deviceContext->IASetVertexBuffers(0,1,&m_VertexBuffer,&stride,&offset);

	//Set the index buffer to active
	deviceContext->IASetIndexBuffer(m_IndexBuffer,DXGI_FORMAT_R32_UINT,0);

	//Set the type of primitive that should be rendered
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

bool CSprite::LoadTexture(ID3D11Device* device, LPSTR textureFile)
{
	bool result;

	m_Texture = new CTexture;
	if(!m_Texture) return false;

	result = m_Texture->LoadTexture(device, textureFile);
	if(!result) return false;

	return true;
}

void CSprite::ReleaseTexture()
{
	if(m_Texture)
	{
		m_Texture->Clean();
		delete m_Texture;
		m_Texture = 0;
	}
}