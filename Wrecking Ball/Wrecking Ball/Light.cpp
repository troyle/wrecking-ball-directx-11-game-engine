#include "Light.h"

void CLight::SetAmbientColour(float r,float g,float b,float a)
{
	m_AmbientColour = D3DXVECTOR4(r,g,b,a);
}

void CLight::SetDiffuseColour(float r, float g, float b, float a)
{
	m_DiffuseColour = D3DXVECTOR4(r,g,b,a);
}

void CLight::SetDirection(float x, float y, float z)
{
	m_Direction = D3DXVECTOR3(x,y,z);
}

void CLight::SetSpecularColour(float r,float g,float b,float a)
{
	m_SpecularColour = D3DXVECTOR4(r,g,b,a);
}

void CLight::SetSpecularPower(float p)
{
	m_SpecularPower = p;
}