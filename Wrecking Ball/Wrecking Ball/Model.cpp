
#include "Model.h"


CModel::CModel()
{
	m_SizeX = m_SizeY = m_SizeZ = 1.0f;
	m_Pos = { 0, 0, 0 };
}

CModel::CModel(CMesh* mesh)
{
	m_Pos.x = m_Pos.y = m_Pos.z = 0;
	m_Rot.x = m_Rot.y = m_Rot.z = 0;
	m_SizeX = m_SizeY = m_SizeZ = 1.0f;
	m_Mesh = mesh;
}

void CModel::SetMesh(CMesh* mesh)
{
	m_Mesh = mesh;
}

CMesh* CModel::GetMesh()
{
	return m_Mesh;
}

void CModel::Clean()
{
	m_Mesh = 0;

	ReleaseTexture();
}

bool CModel::SetTexture(LPSTR texturefile1, LPSTR texturefile2)
{
	bool result = 0;

	//Create texture
	CTexture* texture = new CTexture;
	if(!texture) return false;

	//Load the file into the texture
	//result = texture->LoadTexture(device->GetDevice(),texturefile1,texturefile2);
	if(!result) return false;

	m_Texture = texture;

	return true;
}

bool CModel::SetTexture(CTexture* texture)
{
	m_Texture = texture;
	if(!m_Texture) return false;

	return true;
}

bool CModel::SetSpecular(LPSTR specularfile)
{
	bool result = 0;

	//Create texture
	CTexture* specular = new CTexture;
	if(!specular) return false;

	//Load the file into the texture
	//result = specular->LoadTexture(device->GetDevice(),specularfile,"");
	if(!result) return false;

	m_Specular = specular;

	return true;
}

bool CModel::SetSpecular(CTexture* specular)
{
	m_Specular = specular;
	if(!m_Specular) return false;

	return true;
}

void CModel::ReleaseTexture()
{
	//Release texture
	if(m_Texture)
	{
		m_Texture->Clean();
		delete m_Texture;
		m_Texture = 0;
	}

	if(m_Specular)
	{
		m_Specular->Clean();
		delete m_Specular;
		m_Specular = 0;
	}
}