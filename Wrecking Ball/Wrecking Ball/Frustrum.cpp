#include "Frustrum.h"

void CFrustrum::UpdateFrustrum(float screenDepth,D3DXMATRIX projMatrix,D3DXMATRIX viewMatrix)
{
	float zMinimum, frustrumDepth;
	D3DXMATRIX matrix;

	zMinimum = -projMatrix._43 / projMatrix._33;
	frustrumDepth = screenDepth / (screenDepth - zMinimum);
	projMatrix._33 = frustrumDepth;
	projMatrix._43 = -frustrumDepth * zMinimum;

	D3DXMatrixMultiply(&matrix,&viewMatrix,&projMatrix);

	//Near plane of frustrum
	m_Planes[0].a = matrix._14 + matrix._13;
	m_Planes[0].b = matrix._24 + matrix._23;
	m_Planes[0].c = matrix._34 + matrix._33;
	m_Planes[0].d = matrix._44 + matrix._43;
	D3DXPlaneNormalize(&m_Planes[0],&m_Planes[0]);

	//Far plane of frustrum
	m_Planes[1].a = matrix._14 - matrix._13;
	m_Planes[1].b = matrix._24 - matrix._23;
	m_Planes[1].c = matrix._34 - matrix._33;
	m_Planes[1].d = matrix._44 - matrix._43;
	D3DXPlaneNormalize(&m_Planes[1],&m_Planes[1]);

	//Left plane of frustrum
	m_Planes[2].a = matrix._14 + matrix._11;
	m_Planes[2].b = matrix._24 + matrix._21;
	m_Planes[2].c = matrix._34 + matrix._31;
	m_Planes[2].d = matrix._44 + matrix._41;
	D3DXPlaneNormalize(&m_Planes[2],&m_Planes[2]);

	//Right plane of frustrum
	m_Planes[3].a = matrix._14 - matrix._11;
	m_Planes[3].b = matrix._24 - matrix._21;
	m_Planes[3].c = matrix._34 - matrix._31;
	m_Planes[3].d = matrix._44 - matrix._41;
	D3DXPlaneNormalize(&m_Planes[3],&m_Planes[3]);

	//Top plane of frustrum
	m_Planes[4].a = matrix._14 - matrix._12;
	m_Planes[4].b = matrix._24 - matrix._22;
	m_Planes[4].c = matrix._34 - matrix._32;
	m_Planes[4].d = matrix._44 - matrix._42;
	D3DXPlaneNormalize(&m_Planes[4],&m_Planes[4]);

	//Bottom plane of frustrum
	m_Planes[5].a = matrix._14 + matrix._12;
	m_Planes[5].b = matrix._24 + matrix._22;
	m_Planes[5].c = matrix._34 + matrix._32;
	m_Planes[5].d = matrix._44 + matrix._42;
	D3DXPlaneNormalize(&m_Planes[5],&m_Planes[5]);
}

bool CFrustrum::CheckPoint(float x,float y,float z)
{
	for(int i = 0;i < 6;i++)
		if(D3DXPlaneDotCoord(&m_Planes[i],&D3DXVECTOR3(x,y,z)) < 0.0f)
			return false;

	return true;
}

bool CFrustrum::CheckCube(float xCenter,float yCenter,float zCenter,float xSize,float ySize = -1,float zSize = 0)
{
	if(ySize == -1)
		ySize = zSize = xSize;

	for(int i = 0;i < 6;i++)
	{
		if(D3DXPlaneDotCoord(&m_Planes[i],&D3DXVECTOR3((xCenter - xSize),(yCenter - ySize),(zCenter - zSize))) >= 0.0f)
			continue;
		if(D3DXPlaneDotCoord(&m_Planes[i],&D3DXVECTOR3((xCenter + xSize),(yCenter - ySize),(zCenter - zSize))) >= 0.0f)
			continue;
		if(D3DXPlaneDotCoord(&m_Planes[i],&D3DXVECTOR3((xCenter - xSize),(yCenter + ySize),(zCenter - zSize))) >= 0.0f)
			continue;
		if(D3DXPlaneDotCoord(&m_Planes[i],&D3DXVECTOR3((xCenter + xSize),(yCenter + ySize),(zCenter - zSize))) >= 0.0f)
			continue;
		if(D3DXPlaneDotCoord(&m_Planes[i],&D3DXVECTOR3((xCenter - xSize),(yCenter - ySize),(zCenter + zSize))) >= 0.0f)
			continue;
		if(D3DXPlaneDotCoord(&m_Planes[i],&D3DXVECTOR3((xCenter + xSize),(yCenter - ySize),(zCenter + zSize))) >= 0.0f)
			continue;
		if(D3DXPlaneDotCoord(&m_Planes[i],&D3DXVECTOR3((xCenter - xSize),(yCenter + ySize),(zCenter + zSize))) >= 0.0f)
			continue;
		if(D3DXPlaneDotCoord(&m_Planes[i],&D3DXVECTOR3((xCenter + xSize),(yCenter + ySize),(zCenter + zSize))) >= 0.0f)
			continue;

		return false;
	}
	return true;
}

bool CFrustrum::CheckSphere(float xCenter,float yCenter,float zCenter,float radius)
{
	for(int i = 0;i < 6;i++)
		if(D3DXPlaneDotCoord(&m_Planes[i],&D3DXVECTOR3(xCenter,yCenter,zCenter)) < -radius)
		return false;

	return true;
}