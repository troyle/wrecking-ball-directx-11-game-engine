#include "ModelInterface.h"

IModel::IModel(CModel* model)
{
	m_Model = model;
}

void IModel::SetMesh(CMesh* mesh)
{
}

CMesh* IModel::GetMesh()
{
	return 0;
}

void IModel::SetPosition(float X, float Y, float Z) { m_Model->SetPosition(X, Y, Z); }
D3DXVECTOR3 IModel::GetPosition() { return m_Model->GetPosition(); }

void IModel::MoveX(float X) { m_Model->MoveX(X); }
float IModel::GetX() { return m_Model->GetX(); }

void IModel::MoveY(float Y) { m_Model->MoveY(Y); }
float IModel::GetY() { return m_Model->GetY(); }

void IModel::MoveZ(float Z) { m_Model->MoveZ(Z); }
float IModel::GetZ() { return m_Model->GetZ(); }

void IModel::SetRotation(float X, float Y, float Z) { m_Model->SetRotation(X, Y, Z); }
D3DXVECTOR3 IModel::GetRotation() { return m_Model->GetRotation(); }

void IModel::RotateX(float X) { m_Model->RotateX(X); }
float IModel::GetXRotation() { return m_Model->GetXRotation(); }

void IModel::RotateY(float Y) { m_Model->RotateY(Y); }
float IModel::GetYRotation() { return m_Model->GetYRotation(); }

void IModel::RotateZ(float Z) { m_Model->RotateZ(Z); }
float IModel::GetZRotation() { return m_Model->GetZRotation(); }

bool IModel::SetTexture(LPSTR texturefile1,LPSTR texturefile2)
{
	return 0;
}

bool IModel::SetTexture(CTexture* texture)
{
	return m_Model->SetTexture(texture);
}

bool IModel::SetSpecular(LPSTR specularfile)
{
	return 0;
}

bool IModel::SetSpecular(CTexture* specular)
{
	return 0;
}

ID3D11ShaderResourceView** IModel::GetTexture()
{
	return 0;
}

ID3D11ShaderResourceView** IModel::GetSpecular()
{
	return 0;
}
void IModel::GetMatrix(D3DXMATRIX &worldMatrix)
{
}

void IModel::Clean()
{
}

