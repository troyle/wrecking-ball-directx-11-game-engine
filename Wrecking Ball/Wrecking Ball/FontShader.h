#ifndef FONTSHADER_H
#define FONTSHADER_H

//Include
#include <d3d11.h>
#include <d3dx10math.h>
#include <d3dx11async.h>
#include <fstream>

class CFontShader
{
private:
	struct ConstantBuffer
	{
		D3DXMATRIX worldMatrix;
		D3DXMATRIX viewMatrix;
		D3DXMATRIX projMatrix;
	};
	struct PixelBuffer
	{
		D3DXVECTOR4 pixelColour;
	};

	ID3D11VertexShader* m_VertexShader;
	ID3D11PixelShader*	m_PixelShader;
	ID3D11InputLayout*	m_Layout;
	ID3D11Buffer*		m_ConstantBuffer;
	ID3D11Buffer*		m_PixelBuffer;
	ID3D11SamplerState*	m_SampleState;

	bool InitialiseShader(ID3D11Device*,HWND,LPCSTR,LPCSTR);
	void ShutdownShader();
	void OutputShaderErrorMessage(ID3D10Blob*,HWND,LPCSTR);
	bool SetShaderParameters(ID3D11DeviceContext*,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX,ID3D11ShaderResourceView**,
																								D3DXVECTOR4);
	void RenderShader(ID3D11DeviceContext*,int);
public:
	CFontShader();
	CFontShader(const CFontShader&) {}
	~CFontShader() {}

	bool Initialise(ID3D11Device*,HWND);
	void Clean();
	bool Render(ID3D11DeviceContext*,int,D3DXMATRIX,D3DXMATRIX,D3DXMATRIX,ID3D11ShaderResourceView**,
																								D3DXVECTOR4);
};

#endif