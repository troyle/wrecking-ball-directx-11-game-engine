#ifndef RENDER_TO_TEXTURE_H
#define RENDER_TO_TEXTURE_H

#include <d3d11.h>
#include <D3DX10math.h>

class CToTexture
{

private:
	ID3D11Texture2D* m_RenderTargetTexture;
	ID3D11RenderTargetView* m_RenderTargetView;
	ID3D11ShaderResourceView* m_ShaderResourceView;
	ID3D11Texture2D* m_DepthStencilBuffer;
	ID3D11DepthStencilView* m_DepthStencilView;
	D3D11_VIEWPORT m_Viewport;

	D3DXMATRIX m_ProjMatrix;
	D3DXMATRIX m_OrthoMatrix;

public:
	CToTexture();
	CToTexture(const CToTexture&) {}
	~CToTexture() {}

	bool Initialise(ID3D11Device*,int,int,float,float);
	void Shutdown();

	void SetRenderTarget(ID3D11DeviceContext*);
	void ClearRenderTarget(ID3D11DeviceContext*,float,float,float,float);
	ID3D11ShaderResourceView* GetShaderResourceView();

	void GetProjMatrix(D3DXMATRIX&);
	void GetOrthoMatrix(D3DXMATRIX&);

};

#endif