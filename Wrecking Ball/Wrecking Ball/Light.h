#ifndef LIGHTING_H
#define LIGHTING_H

#include <D3DX10math.h>

class CLight
{
private:
	D3DXVECTOR4 m_AmbientColour;
	D3DXVECTOR4 m_DiffuseColour;
	D3DXVECTOR3 m_Direction;
	D3DXVECTOR4 m_SpecularColour;
	float		m_SpecularPower;
public:
	CLight(){}
	CLight(const CLight&){}
	~CLight(){}

	void SetAmbientColour(float, float, float, float);
	void SetDiffuseColour(float, float, float, float);
	void SetDirection(float, float, float);
	void SetSpecularColour(float, float, float, float);
	void SetSpecularPower(float);

	D3DXVECTOR4 GetAmbientColour()	{ return m_AmbientColour; }
	D3DXVECTOR4 GetDiffuseColour()	{ return m_DiffuseColour; }
	D3DXVECTOR3 GetDirection()		{ return m_Direction; }
	D3DXVECTOR4 GetSpecularColour() { return m_SpecularColour; }
	float		GetSpecularPower()	{ return m_SpecularPower; }
};

#endif