#ifndef FONT_H
#define FONT_H

#include "Texture.h"

#include <d3d11.h>
#include <D3DX10math.h>
#include <fstream>

class CFontHandler
{
private:
	struct Font
	{
		float left, right;
		int size;
	};

	struct Vertex
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR2 UV;
	};

	Font* m_Font;
	CTexture* m_Texture;

	bool LoadFontData(char*);
	void ReleaseFontData();
	bool LoadTexture(ID3D11Device*,LPSTR);
	void ReleaseTexture();
public:
	CFontHandler();
	CFontHandler(const CFontHandler&){}
	~CFontHandler(){}

	bool Initialise(ID3D11Device*,char*,LPSTR);
	void Clean();

	ID3D11ShaderResourceView** GetTexture() { return m_Texture->GetTexture(); }
	void BuildVertexArray(void*,char*,float,float);
};

#endif