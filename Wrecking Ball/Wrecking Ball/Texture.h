#ifndef TEXTURE_H
#define TEXTURE_H

//Includes
#include <d3d11.h>
#include <D3DX11tex.h>

class CTexture
{
private:
	ID3D11ShaderResourceView* m_Texture[3];
public:
	CTexture();
	CTexture(const CTexture&){}
	~CTexture(){}

	bool LoadTexture(ID3D11Device*,LPSTR,LPSTR = "",LPSTR = "");
	void Clean();

	ID3D11ShaderResourceView** GetTexture()	{ return m_Texture; }
};

#endif