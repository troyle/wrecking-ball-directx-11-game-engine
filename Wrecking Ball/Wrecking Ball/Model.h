#ifndef MODEL_H
#define MODEL_H

//#include "ModelAbstract.h"
#include "3DObject.h"
#include "Texture.h"
#include "Mesh.h"

#include <D3DX10math.h>
#include <stdlib.h>

class CModel: public I3DObject//, public IModel
{
private:
	float m_SizeX, m_SizeY, m_SizeZ;
	CTexture* m_Texture;
	CTexture* m_Specular;
	CMesh* m_Mesh;

	void ReleaseTexture();

public:
	CModel();
	CModel(CMesh* mesh);
	CModel(const CModel&){}
	~CModel() {}

	void SetMesh(CMesh*);
	CMesh* GetMesh();

	bool SetTexture(LPSTR texture1, LPSTR texture2 = "");
	bool SetTexture(CTexture* texture);

	bool SetSpecular(LPSTR specular);
	bool SetSpecular(CTexture* specular);

	ID3D11ShaderResourceView** GetTexture() { return m_Texture->GetTexture(); }
	ID3D11ShaderResourceView** GetSpecular() { if(m_Specular) return m_Specular->GetTexture(); else return 0; }
	
	void Clean();
};

#endif